CREATE PROCEDURE ConsultarCitaEstado(in nombre_estadoP varchar(50))
begin 
select
cita.`codigo_cita` AS CODIGO,
cita.`fecha_cita` AS FECHA,
cita.`hora_cita` AS HORA,
cita.`valor_cita` AS VALOR,
operario.nombre_operario AS OPERARIO,
paciente.nombre_paciente AS PACIENTE,
medico.nombre_medico AS MEDICO,
estado.nombre_estado AS ESTADO  
from   cita,estado,medico,paciente,operario
where cita.`fk_operario` = operario.cedula_operario AND
cita.`fk_paciente` = paciente.cedula_paciente AND
cita.`fk_medico` = medico.cedula_medico AND 
cita.`fk_estado` = estado.codigo_estado AND
estado.nombre_estado = cita.nombre_estadoP ;
end;//

CREATE PROCEDURE ConsultarDiagnosticoEstado(in nombre_estadoP varchar(50))
begin 
select
diagnostico.`codigo_diagnostico` AS CODIGO,
diagnostico.`diagnostico_medico` AS DIAGNOSTICO,
diagnostico.`diagnostico_enfermeria` AS DESCRIPCION,
diagnostico.`fecha_diagnostico` AS FECHA,
medicamento.nombre_medicamento AS MEDICAMENTO,
medico.nombre_medico AS MEDICO,
estado.nombre_estado AS ESTADO
FROM	
diagnostico,medicamento,medico,estado
WHERE
diagnostico.`fk_medicamento` = medicamento.codigo_medicamento AND
diagnostico.fk_medico = medico.cedula_medico AND
diagnostico.fk_estado = estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end;//

CREATE PROCEDURE ConsultarfacturacompraEstado(in nombre_estadoP varchar(50))
begin 
select
 
factura_compra.`codigo_factura_compra` AS CODIGO,
factura_compra.`fecha_factura_compra` AS FECHACOMPRA,
factura_compra.`total_compra` AS TOTAL,
forma_pago.nombre_forma_pago AS FORMAPAGO,
operario.nombre_operario AS OPERARIO,
proveedor.nombre_proveedor AS PROVEEDOR,
estado.nombre_estado AS ESTADO
FROM	
factura_compra,forma_pago,operario,proveedor,estado
WHERE
factura_compra.`fk_forma_pago` = forma_pago.codigo_forma_pago AND
factura_compra.`fk_operario` = operario.cedula_operario AND
factura_compra.`fk_proveedor`= proveedor.cedula_proveedor AND 
factura_compra.`fk_estado`= estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end;//

CREATE PROCEDURE ConsultarfacturaventaEstado(in nombre_estadoP varchar(50))
begin 
select
factura_venta.`codigo_factura_venta` AS CODIGO,
factura_venta.`fecha_factura_venta` AS FECHAVENTA,
factura_venta.`total_venta` AS TOTAL,
forma_pago.nombre_forma_pago AS FORMAPAGO,
cita.codigo_cita AS NOCITA,
estado.nombre_estado AS ESTADO
FROM	
factura_venta,forma_pago,cita,estado
WHERE
factura_venta.`fk_forma_pago` = forma_pago.codigo_forma_pago AND
factura_venta.`fk_cita` = cita.codigo_cita AND 
factura_venta.`fk_estado`= estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end;//

CREATE PROCEDURE ConsultarformapagoEstado(in nombre_estadoP varchar(50))
begin 
select
forma_pago.`codigo_forma_pago` AS CODIGO,
forma_pago.`nombre_forma_pago` AS NOMBRE,
estado.nombre_estado AS ESTADO
FROM	
forma_pago,estado
WHERE
forma_pago.`fk_estado`= estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end;//

CREATE PROCEDURE ConsultarhistorialEstado(in nombre_estadoP varchar(50))
begin 
select
historial.`codigo_historial` AS CODIGO,
historial.antecedentes AS ANTECEDENTES,
historial.procedimientoActual AS PROCEDIMIENTO,
historial.alergias AS ALERGIAS,
medico.nombre_medico AS MEDICO,
paciente.nombre_paciente AS PACIENTE,
estado.nombre_estado AS ESTADO
FROM	
historial,medico,paciente,estado
WHERE
historial.`fk_medico`=  medico.cedula_medico AND
historial.fk_paciente = paciente.cedula_paciente AND
historial.fk_estado = estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end;//

CREATE PROCEDURE ConsultarinventariomedicamentoEstado(in nombre_estadoP varchar(50))
begin 
select
inventario_medicamento.`codigo_inventario` AS CODIGO,
inventario_medicamento.`nombre_inventario` AS NOMBRE,
estado.nombre_estado AS ESTADO
FROM	
inventario_medicamento,estado
WHERE
inventario_medicamento.fk_estado = estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end;//

CREATE PROCEDURE ConsultarmedicamentoEstado(in nombre_estadoP varchar(50))
begin 
select
medicamento.`codigo_medicamento` AS CODIGO,
medicamento.`nombre_medicamento` AS NOMBRE,
medicamento.`descripcion_medicamento` AS DESCRIPCION,
medicamento.`contraindicaciones_medicamento` AS CONTRAINDICACIONES,
medicamento.`sigla_medicamento`AS SIGLA,
medicamento.`fecha_elaboracion` AS ELABORADO,
medicamento.`fecha_vencimiento` AS VENCE,
medicamento.`valor_medicamento` AS VALOR,
estado.nombre_estado AS ESTADO
FROM	
medicamento,estado
WHERE
medicamento.fk_estado = estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end;//

CREATE PROCEDURE ConsultarmedicoEstado(in nombre_estadoP varchar(50))
begin 
select
medico.`cedula_medico`AS CEDULA,
medico.`nombre_medico` AS NOMBRE,
medico.`apellido_medico` AS APELLIDO,
medico.`fecha_nacimiento` AS NACIMIENTO,
medico.`correo_medico`AS CORREO,
medico.`telefono_medico` AS TELEFONO,
medico.`direccion_medico` AS DIRECCION,
medico.`genero_medico` AS MEDICO,
estado.nombre_estado AS ESTADO
FROM	
medico,estado
WHERE
medico.fk_estado = estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end;//

CREATE PROCEDURE ConsultaroperarioEstado(in nombre_estadoP varchar(50))
begin 
select
operario.`cedula_operario`AS CEDULA,
operario.`nombre_operario` AS NOMBRE,
operario.`apellido_operario` AS APELLIDO,
operario.`fecha_nacimiento` AS NACIMIENTO,
operario.`correo_operario`AS CORREO,
operario.`telefono_operario` AS TELEFONO,
operario.`direccion_operario` AS DIRECCION,
operario.`genero_operario` AS OPERARIO,
estado.nombre_estado AS ESTADO
FROM	
operario,estado
WHERE
operario.fk_estado = estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end;//

CREATE PROCEDURE ConsultarproveedorEstado(in nombre_estadoP varchar(50))
begin 
select
proveedor.`cedula_proveedor`AS CEDULA,
proveedor.`nombre_proveedor` AS NOMBRE,
proveedor.`apellido_proveedor` AS APELLIDO,
proveedor.`correo_proveedor`AS CORREO,
proveedor.`telefono_proveedor` AS TELEFONO,
proveedor.`genero_proveedor` AS PROVEEDOR,
estado.nombre_estado AS ESTADO
FROM	
proveedor,estado
WHERE
proveedor.fk_estado = estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end;//

CREATE PROCEDURE ConsultarpacienteEstado(in nombre_estadoP varchar(50))
begin 
select
paciente.`cedula_paciente`AS CEDULA,
paciente.`nombre_paciente` AS NOMBRE,
paciente.`apellido_paciente` AS APELLIDO,
paciente.`fecha_nacimiento` AS NACIMIENTO,
paciente.`correo_paciente`AS CORREO,
paciente.`telefono_paciente` AS TELEFONO,
paciente.`direccion_paciente` AS DIRECCION,
paciente.`genero_paciente` AS PACIENTE,
estado.nombre_estado AS ESTADO
FROM	
paciente,estado
WHERE
paciente.fk_estado = estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end;//