CREATE PROCEDURE AddAsignarInventario(in codigo_asignar_inventarioP bigint, 
in cantidad_existenteP bigint, in stockP bigint, in fk_inventario_medicamentoP bigint,
in fk_medicamentoP bigint)
begin insert into asignar_inventario
(codigo_asignar_inventario, cantidad_existente, stock, fk_inventario_medicamento, fk_medicamento) 
values(codigo_asignar_inventarioP, cantidad_existenteP, stockP, fk_inventario_medicamentoP, fk_medicamentoP); end;//


CREATE PROCEDURE AddCita(in codigo_citaP bigint, 
in fecha_citaP date, in hora_citaP time, in valor_citaP bigint,
in fk_operarioP bigint, in fk_pacienteP bigint, in fk_medicoP bigint,
in fk_estadoP bigint)
begin insert into cita
(codigo_cita, fecha_cita, hora_cita, valor_cita ,
fk_operario , fk_paciente , fk_medico ,
fk_estado )
values(codigo_citaP, fecha_citaP, hora_citaP, valor_citaP ,
fk_operarioP , fk_pacienteP , fk_medicoP ,
fk_estadoP); end;//


CREATE PROCEDURE AddDetalleFacturaCompra(in codigo_detalle_factura_compraP bigint, 
in cantidad_entradaP bigint, in subtotalP bigint, in fk_factura_compraP bigint,
in fk_medicamentoP bigint)
begin insert into detalle_factura_compra
(codigo_detalle_factura_compra,
cantidad_entrada, subtotal, fk_factura_compra,
fk_medicamento)
values(codigo_detalle_factura_compraP,
cantidad_entradaP, subtotalP, fk_factura_compraP,
fk_medicamentoP); end;//


CREATE PROCEDURE AddDetalleFacturaVenta(in codigo_detalle_factura_ventaP bigint, 
in cantidad_salidaP bigint, in subtotalP bigint, in fk_factura_ventaP bigint,
in fk_medicamentoP bigint)
begin insert into detalle_factura_venta
(codigo_detalle_factura_venta,
cantidad_salida, subtotal, fk_factura_venta,
fk_medicamento)
values(codigo_detalle_factura_ventaP,
cantidad_salidaP, subtotalP, fk_factura_ventaP,
fk_medicamentoP); end;//


CREATE PROCEDURE AddDiagnostico(in codigo_diagnosticoP bigint, 
in diagnostico_medicoP varchar(50), in diagnostico_enfermeriaP varchar(50), 
in fecha_diagnosticoP date, in fk_medicamentoP bigint,
in fk_medicoP bigint, in fk_estadoP bigint)
begin insert into diagnostico
(codigo_diagnostico, 
diagnostico_medico, diagnostico_enfermeria , 
fecha_diagnostico, fk_medicamento ,
fk_medico , fk_estado)
values(codigo_diagnosticoP, 
diagnostico_medicoP, diagnostico_enfermeriaP , 
fecha_diagnosticoP, fk_medicamentoP ,
fk_medicoP , fk_estadoP); end;//


CREATE PROCEDURE AddEstado(in codigo_estadooP bigint, 
in nombre_estadoP varchar(50))
begin insert into estado
(codigo_estado, nombre_estado)
values(codigo_estadoP, nombre_estadoP); end;//

CREATE PROCEDURE AddFacturaCompra(in codigo_factura_compraP bigint, 
in fecha_factura_compraP date, in total_compraP bigint, 
in fk_forma_pagoP bigint, in fk_operarioP bigint,
in fk_proveedorP bigint, in fk_estadoP bigint)
begin insert into factura_compra
(codigo_factura_compra, 
fecha_factura_compra ,  total_compra ,
fk_forma_pago , fk_operario ,
fk_proveedor ,  fk_estado )
values(codigo_factura_compraP, 
fecha_factura_compraP ,  total_compraP ,
fk_forma_pagoP , fk_operarioP ,
fk_proveedorP ,  fk_estadoP); end;//


CREATE PROCEDURE AddFacturaVenta(in codigo_factura_ventaP bigint, 
in fecha_factura_ventaP date, in total_ventaP bigint, 
in fk_forma_pagoP bigint, in fk_citaP bigint, in fk_estadoP bigint)
begin insert into factura_venta
(codigo_factura_venta ,
fecha_factura_venta , total_venta ,
fk_forma_pago , fk_cita, fk_estado )
values(codigo_factura_ventaP ,
fecha_factura_ventaP , total_ventaP ,
fk_forma_pagoP , fk_citaP , fk_estadoP ); end;//


CREATE PROCEDURE AddFormaPago(in codigo_forma_pagoP bigint, 
in nombre_forma_pagoP varchar(50), in fk_estadoP bigint )
begin insert into forma_pago
(codigo_forma_pago ,
nombre_forma_pago ,  fk_estado )
values(codigo_forma_pagoP ,
nombre_forma_pagoP ,  fk_estadoP); end;//


CREATE PROCEDURE AddHistorial(in codigo_historialP bigint, 
in antecedentesP varchar(50), in procedimientoActualP varchar(50),
in alergiasP varchar(50), in fk_medicoP bigint, 
in fk_pacienteP bigint, in fk_estadoP bigint )
begin insert into historial
(codigo_historial ,
antecedentes ,  procedimientoActual ,
alergias , fk_medico , 
fk_paciente , fk_estado )
values(codigo_historialP ,
antecedentesP ,  procedimientoActualP ,
alergiasP , fk_medicoP , 
fk_pacienteP , fk_estadoP); end;//


CREATE PROCEDURE AddInventarioMedicamento(in codigo_inventarioP bigint, 
in nombre_inventarioP varchar(50), in fk_estadoP bigint )
begin insert into inventario_medicamento
(codigo_inventario ,
nombre_inventario ,  fk_estado )
values(codigo_inventarioP ,
nombre_inventarioP ,  fk_estadoP); end;//


CREATE PROCEDURE AddMedicamento(in codigo_medicamentoP bigint, 
in nombre_medicamentoP varchar(50), in descripcion_medicamentoP varchar(50),
in contraindicaciones_medicamentoP varchar(50), in sigla_medicamentoP varchar(50), 
in fecha_elaboracionP date, in fecha_vencimientoP date,
in valor_medicamentoP bigint, in fk_estadoP bigint )
begin insert into medicamento
(codigo_medicamento ,
 nombre_medicamento ,  descripcion_medicamento ,
 contraindicaciones_medicamento ,  sigla_medicamento , 
 fecha_elaboracion , fecha_vencimiento ,
 valor_medicamento,  fk_estado )
values(codigo_medicamentoP ,
 nombre_medicamentoP ,  descripcion_medicamentoP ,
 contraindicaciones_medicamentoP ,  sigla_medicamentoP , 
 fecha_elaboracionP , fecha_vencimientoP ,
 valor_medicamentoP ,  fk_estadoP); end;//


CREATE PROCEDURE AddMedico(in cedula_medicoP bigint, 
in nombre_medicoP varchar(50), in apellido_medicoP varchar(50), 
in fecha_nacimientoP date, in correo_medicoP varchar(50), 
in telefono_medicoP bigint, in direccion_medicoP varchar(50),
in genero_medicoP varchar(50), in fk_estadoP bigint )
begin insert into medico
(cedula_medico, 
 nombre_medico,  apellido_medico , 
 fecha_nacimiento ,  correo_medico , 
 telefono_medico , direccion_medico ,
 genero_medico ,  fk_estado  )
values(cedula_medicoP , 
 nombre_medicoP ,  apellido_medicoP , 
 fecha_nacimientoP ,  correo_medicoP , 
 telefono_medicoP , direccion_medicoP ,
 genero_medicoP ,  fk_estadoP ); end;//


CREATE PROCEDURE AddOperario(in cedula_operarioP bigint, 
in nombre_operarioP varchar(50), in apellido_operarioP varchar(50), 
in fecha_nacimientoP date, in correo_operarioP varchar(50), 
in telefono_operarioP bigint, in direccion_operarioP varchar(50),
in genero_operarioP varchar(50), in fk_estadoP bigint )
begin insert into operario
(cedula_operario, 
 nombre_operario,  apellido_operario , 
 fecha_nacimiento ,  correo_operario , 
 telefono_operario , direccion_operario ,
 genero_operario ,  fk_estado  )
values(cedula_operarioP , 
 nombre_operarioP ,  apellido_operarioP , 
 fecha_nacimientoP ,  correo_operarioP , 
 telefono_operarioP , direccion_operarioP ,
 genero_operarioP ,  fk_estadoP ); end;//


CREATE PROCEDURE AddPaciente(in cedula_pacienteP bigint, 
in nombre_pacienteP varchar(50), in apellido_pacienteP varchar(50), 
in fecha_nacimientoP date, in correo_pacienteP varchar(50), 
in telefono_pacienteP bigint, in direccion_paciente varchar(50),
in genero_pacienteP varchar(50), in fk_estadoP bigint )
begin insert into paciente
(cedula_paciente, 
 nombre_paciente,  apellido_paciente , 
 fecha_nacimiento ,  correo_paciente , 
 telefono_paciente , direccion_paciente ,
 genero_paciente ,  fk_estado  )
values(cedula_pacienteP , 
 nombre_pacienteP ,  apellido_pacienteP , 
 fecha_nacimientoP ,  correo_pacienteP , 
 telefono_pacienteP , direccion_pacienteP ,
 genero_pacienteP ,  fk_estadoP ); end;//


CREATE PROCEDURE AddProveedor(in cedula_proveedorP bigint, 
in nombre_proveedorP varchar(50), in apellido_proveedorP varchar(50), 
in correo_proveedorP varchar(50), 
in telefono_proveedorP bigint, 
in genero_proveedorP varchar(50), in fk_estadoP bigint )
begin insert into proveedor
(cedula_proveedor, 
 nombre_proveedor ,  apellido_proveedor , 
 correo_proveedor , 
 telefono_proveedor , 
 genero_proveedor ,  fk_estado  )
values(cedula_proveedorP, 
 nombre_proveedorP ,  apellido_proveedorP , 
 correo_proveedorP , 
 telefono_proveedorP , 
 genero_proveedorP ,  fk_estadoP ); end;//


CREATE PROCEDURE ModificarAsignarInventario(in codigo_asignar_inventarioP bigint, 
in cantidad_existenteP bigint, in stockP bigint, in fk_inventario_medicamentoP bigint,
in fk_medicamentoP bigint)
begin update asignar_inventario
set cantidad_existente=cantidad_existenteP, stock= stockP,  fk_inventario_medicamento=fk_inventario_medicamentoP,  
fk_medicamento= fk_medicamentoP where codigo_asignar_inventario = codigo_asignar_inventarioP; end;//


CREATE PROCEDURE ModificarCita(in codigo_citaP bigint, 
in fecha_citaP date, in hora_citaP time, in valor_citaP bigint,
in fk_operarioP bigint, in fk_pacienteP bigint, in fk_medicoP bigint,
in fk_estadoP bigint)
begin update cita
set fecha_cita= fecha_citaP, hora_cita = hora_citaP,  valor_cita = valor_citaP ,
fk_operario = fk_operarioP , fk_paciente = fk_pacienteP , fk_medico = fk_medicoP ,
fk_estado = fk_estadoP where codigo_cita=codigo_citaP; end;//



CREATE PROCEDURE ModificarDetalleFacturaCompra(in codigo_detalle_factura_compraP bigint, 
in cantidad_entradaP bigint, in subtotalP bigint, in fk_factura_compraP bigint,
in fk_medicamentoP bigint)
begin update detalle_factura_compra
set
cantidad_entrada=cantidad_entradaP,  subtotal=subtotalP,  fk_factura_compra=fk_factura_compraP,
fk_medicamento= fk_medicamentoP where  codigo_detalle_factura_compra = codigo_detalle_factura_compraP; end;//



CREATE PROCEDURE ModificarDetalleFacturaVenta(in codigo_detalle_factura_ventaP bigint, 
in cantidad_salidaP bigint, in subtotalP bigint, in fk_factura_ventaP bigint,
in fk_medicamentoP bigint)
begin update detalle_factura_venta
set 
cantidad_salida = cantidad_salidaP,  subtotal = subtotalP, fk_factura_venta = fk_factura_ventaP,
fk_medicamento=fk_medicamentoP where codigo_detalle_factura_venta=codigo_detalle_factura_ventaP; end;//



CREATE PROCEDURE ModificarDiagnostico(in codigo_diagnosticoP bigint, 
in diagnostico_medicoP varchar(50), in diagnostico_enfermeriaP varchar(50), 
in fecha_diagnosticoP date, in fk_medicamentoP bigint,
in fk_medicoP bigint, in fk_estadoP bigint)
begin update diagnostico
set 
diagnostico_medico=diagnostico_medicoP,  diagnostico_enfermeria = diagnostico_enfermeriaP , 
fecha_diagnostico = fecha_diagnosticoP, fk_medicamento=fk_medicamentoP ,
fk_medico=fk_medicoP , fk_estado=fk_estadoP where codigo_diagnostico = codigo_diagnosticoP; end;//


CREATE PROCEDURE ModificarEstado(in codigo_estadooP bigint, 
in nombre_estadoP varchar(50))
begin update estado
set nombre_estado=nombre_estadoP 
where codigo_estado=codigo_estadoP; end;//


CREATE PROCEDURE ModificarFacturaCompra(in codigo_factura_compraP bigint, 
in fecha_factura_compraP date, in total_compraP bigint, 
in fk_forma_pagoP bigint, in fk_operarioP bigint,
in fk_proveedorP bigint, in fk_estadoP bigint)
begin update factura_compra
set 
fecha_factura_compra = fecha_factura_compraP ,  total_compra = total_compraP,
fk_forma_pago= fk_forma_pagoP  , fk_operario=fk_operarioP  ,
fk_proveedor=fk_proveedorP  ,  fk_estado=fk_estadoP 
where codigo_factura_compra=codigo_factura_compraP; end;//


CREATE PROCEDURE ModificarFacturaVenta(in codigo_factura_ventaP bigint, 
in fecha_factura_ventaP date, in total_ventaP bigint, 
in fk_forma_pagoP bigint, in fk_citaP bigint, in fk_estadoP bigint)
begin update factura_venta
set 
fecha_factura_venta = fecha_factura_ventaP , total_venta= total_ventaP  ,
fk_forma_pago=fk_forma_pagoP  , fk_cita=fk_citaP,  fk_estado = fk_estadoP 
where codigo_factura_venta= codigo_factura_ventaP; end;//


CREATE PROCEDURE ModificarFormaPago(in codigo_forma_pagoP bigint, 
in nombre_forma_pagoP varchar(50), in fk_estadoP bigint )
begin update forma_pago
set 
nombre_forma_pago = nombre_forma_pagoP ,  fk_estado = fk_estadoP 
where codigo_forma_pago=codigo_forma_pagoP; end;//


CREATE PROCEDURE ModificarHistorial(in codigo_historialP bigint, 
in antecedentesP varchar(50), in procedimientoActualP varchar(50),
in alergiasP varchar(50), in fk_medicoP bigint, 
in fk_pacienteP bigint, in fk_estadoP bigint )
begin update historial
set 
antecedentes = antecedentesP ,  procedimientoActual = procedimientoActualP ,
alergias = alergiasP , fk_medico = fk_medicoP , 
fk_paciente = fk_pacienteP , fk_estado = fk_estadoP 
where codigo_historial=codigo_historialP; end;//


CREATE PROCEDURE ModificarInventarioMedicamento(in codigo_inventarioP bigint, 
in nombre_inventarioP varchar(50), in fk_estadoP bigint )
begin update inventario_medicamento
set
nombre_inventario = nombre_inventarioP ,  fk_estado = fk_estadoP 
where codigo_inventario=codigo_inventarioP; end;//


CREATE PROCEDURE ModificarMedicamento(in codigo_medicamentoP bigint, 
in nombre_medicamentoP varchar(50), in descripcion_medicamentoP varchar(50),
in contraindicaciones_medicamentoP varchar(50), in sigla_medicamentoP varchar(50), 
in fecha_elaboracionP date, in fecha_vencimientoP date,
in valor_medicamentoP bigint, in fk_estadoP bigint )
begin update medicamento
set
 nombre_medicamento = nombre_medicamentoP ,  descripcion_medicamento = descripcion_medicamentoP ,
 contraindicaciones_medicamento = contraindicaciones_medicamentoP ,  sigla_medicamento = sigla_medicamentoP , 
 fecha_elaboracion = fecha_elaboracionP , fecha_vencimiento = fecha_vencimientoP ,
 valor_medicamento=valor_medicamentoP,    fk_estado = fk_estadoP
 where codigo_medicamento= codigo_medicamentoP ; end;//


CREATE PROCEDURE ModificarMedico(in cedula_medicoP bigint, 
in nombre_medicoP varchar(50), in apellido_medicoP varchar(50), 
in fecha_nacimientoP date, in correo_medicoP varchar(50), 
in telefono_medicoP bigint, in direccion_medicoP varchar(50),
in genero_medicoP varchar(50), in fk_estadoP bigint )
begin update medico
set
 nombre_medico = nombre_medicoP, apellido_medico = apellido_medicoP , 
 fecha_nacimiento = fecha_nacimientoP  ,  correo_medico = correo_medicoP , 
 telefono_medico = telefono_medicoP , direccion_medico = direccion_medicoP ,
 genero_medico = genero_medicoP ,  fk_estado = fk_estadoP  
where cedula_medico=cedula_medicoP; end;//


CREATE PROCEDURE ModificarOperario(in cedula_operarioP bigint, 
in nombre_operarioP varchar(50), in apellido_operarioP varchar(50), 
in fecha_nacimientoP date, in correo_operarioP varchar(50), 
in telefono_operarioP bigint, in direccion_operarioP varchar(50),
in genero_operarioP varchar(50), in fk_estadoP bigint )
begin update operario
set 
 nombre_operario = nombre_operarioP,    apellido_operario = apellido_operarioP , 
 fecha_nacimiento= fecha_nacimientoP  ,  correo_operario = correo_operarioP , 
 telefono_operario=telefono_operarioP  , direccion_operario = direccion_operarioP ,
 genero_operario = genero_operarioP ,  fk_estado = fk_estadoP  
 where cedula_operario=cedula_operarioP; end;//


CREATE PROCEDURE ModificarPaciente(in cedula_pacienteP bigint, 
in nombre_pacienteP varchar(50), in apellido_pacienteP varchar(50), 
in fecha_nacimientoP date, in correo_pacienteP varchar(50), 
in telefono_pacienteP bigint, in direccion_paciente varchar(50),
in genero_pacienteP varchar(50), in fk_estadoP bigint )
begin update paciente
set
 nombre_paciente = nombre_pacienteP,   apellido_paciente=apellido_pacienteP  , 
 fecha_nacimiento=fecha_nacimientoP  ,  correo_paciente=correo_pacienteP   , 
 telefono_paciente=telefono_pacienteP  , direccion_paciente=direccion_pacienteP  ,
 genero_paciente=genero_pacienteP  ,  fk_estado=fk_estadoP  
 where cedula_paciente= cedula_pacienteP ; end;//


CREATE PROCEDURE ModificarProveedor(in cedula_proveedorP bigint, 
in nombre_proveedorP varchar(50), in apellido_proveedorP varchar(50), 
in correo_proveedorP varchar(50), 
in telefono_proveedorP bigint, 
in genero_proveedorP varchar(50), in fk_estadoP bigint )
begin update proveedor
set
 nombre_proveedor=nombre_proveedorP  ,  apellido_proveedor = apellido_proveedorP , 
 correo_proveedor = correo_proveedorP , 
 telefono_proveedor = telefono_proveedorP , 
 genero_proveedor = genero_proveedorP ,  fk_estado = fk_estadoP  
 where cedula_proveedor=cedula_proveedorP; end;//


CREATE PROCEDURE ConsultarAsignarInventario(in codigo_asignar_inventarioP bigint)
begin select * from asignar_inventario
where codigo_asignar_inventario = codigo_asignar_inventarioP; end;//


CREATE PROCEDURE ConsultarCita(in codigo_citaP bigint)
begin select * from  cita
where codigo_cita=codigo_citaP; end;//



CREATE PROCEDURE ConsultarDetalleFacturaCompra(in codigo_detalle_factura_compraP bigint)
begin select * from  detalle_factura_compra
where  codigo_detalle_factura_compra = codigo_detalle_factura_compraP; end;//



CREATE PROCEDURE ConsultarDetalleFacturaVenta(in codigo_detalle_factura_ventaP bigint)
begin select * from detalle_factura_venta
where codigo_detalle_factura_venta=codigo_detalle_factura_ventaP; end;//



CREATE PROCEDURE ConsultarDiagnostico(in codigo_diagnosticoP bigint)
begin select * from  diagnostico
where codigo_diagnostico = codigo_diagnosticoP; end;//


CREATE PROCEDURE ConsultarEstado(in codigo_estadooP bigint)
begin select * from estado
where codigo_estado=codigo_estadoP; end;//


CREATE PROCEDURE ConsultarFacturaCompra(in codigo_factura_compraP bigint)
begin select * from factura_compra
where codigo_factura_compra=codigo_factura_compraP; end;//


CREATE PROCEDURE ConsultarFacturaVenta(in codigo_factura_ventaP bigint)
begin select * from factura_venta
where codigo_factura_venta= codigo_factura_ventaP; end;//


CREATE PROCEDURE ConsultarFormaPago(in codigo_forma_pagoP bigint)
begin select * from forma_pago
where codigo_forma_pago=codigo_forma_pagoP; end;//


CREATE PROCEDURE ConsultarHistorial(in codigo_historialP bigint)
begin select * from historial
where codigo_historial=codigo_historialP; end;//


CREATE PROCEDURE ConsultarInventarioMedicamento(in codigo_inventarioP bigint)
begin select * from inventario_medicamento
where codigo_inventario=codigo_inventarioP; end;//


CREATE PROCEDURE ConsultarMedicamento(in codigo_medicamentoP bigint)
begin select * from medicamento
 where codigo_medicamento= codigo_medicamentoP ; end;//


CREATE PROCEDURE ConsultarMedico(in cedula_medicoP bigint)
begin select * from medico 
where cedula_medico=cedula_medicoP; end;//


CREATE PROCEDURE ConsultarOperario(in cedula_operarioP bigint )
begin select * from operario
 where cedula_operario=cedula_operarioP; end;//


CREATE PROCEDURE ConsultarPaciente(in cedula_pacienteP bigint)
begin select * from paciente
 where cedula_paciente= cedula_pacienteP ; end;//


CREATE PROCEDURE ConsultarProveedor(in cedula_proveedorP bigint )
begin select * from proveedor
 where cedula_proveedor=cedula_proveedorP; end;//

CREATE PROCEDURE EliminarAsignarInventario(in codigo_asignar_inventarioP bigint)
begin delete from asignar_inventario
where codigo_asignar_inventario = codigo_asignar_inventarioP; end;//


CREATE PROCEDURE EliminarCita(in codigo_citaP bigint)
begin delete from  cita
where codigo_cita=codigo_citaP; end;//



CREATE PROCEDURE EliminarDetalleFacturaCompra(in codigo_detalle_factura_compraP bigint)
begin delete from  detalle_factura_compra
where  codigo_detalle_factura_compra = codigo_detalle_factura_compraP; end;//



CREATE PROCEDURE EliminarDetalleFacturaVenta(in codigo_detalle_factura_ventaP bigint)
begin delete from detalle_factura_venta
where codigo_detalle_factura_venta=codigo_detalle_factura_ventaP; end;//



CREATE PROCEDURE EliminarDiagnostico(in codigo_diagnosticoP bigint)
begin delete from  diagnostico
where codigo_diagnostico = codigo_diagnosticoP; end;//


CREATE PROCEDURE EliminarEstado(in codigo_estadooP bigint)
begin delete from estado
where codigo_estado=codigo_estadoP; end;//


CREATE PROCEDURE EliminarFacturaCompra(in codigo_factura_compraP bigint)
begin delete from factura_compra
where codigo_factura_compra=codigo_factura_compraP; end;//


CREATE PROCEDURE EliminarFacturaVenta(in codigo_factura_ventaP bigint)
begin delete from factura_venta
where codigo_factura_venta= codigo_factura_ventaP; end;//


CREATE PROCEDURE EliminarFormaPago(in codigo_forma_pagoP bigint)
begin delete from forma_pago
where codigo_forma_pago=codigo_forma_pagoP; end;//


CREATE PROCEDURE EliminarHistorial(in codigo_historialP bigint)
begin delete from historial
where codigo_historial=codigo_historialP; end;//


CREATE PROCEDURE EliminarInventarioMedicamento(in codigo_inventarioP bigint)
begin delete from inventario_medicamento
where codigo_inventario=codigo_inventarioP; end;//


CREATE PROCEDURE EliminarMedicamento(in codigo_medicamentoP bigint)
begin delete from medicamento
 where codigo_medicamento= codigo_medicamentoP ; end;//


CREATE PROCEDURE EliminarMedico(in cedula_medicoP bigint)
begin delete from medico 
where cedula_medico=cedula_medicoP; end;//


CREATE PROCEDURE EliminarOperario(in cedula_operarioP bigint )
begin delete from operario
 where cedula_operario=cedula_operarioP; end;//


CREATE PROCEDURE EliminarPaciente(in cedula_pacienteP bigint)
begin delete from paciente
 where cedula_paciente= cedula_pacienteP ; end;//


CREATE PROCEDURE EliminarProveedor(in cedula_proveedorP bigint )
begin delete from proveedor
 where cedula_proveedor=cedula_proveedorP; end;//