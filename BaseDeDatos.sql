-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: consultorio
-- ------------------------------------------------------
-- Server version	5.5.8-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `asignar_inventario`
--

DROP TABLE IF EXISTS `asignar_inventario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignar_inventario` (
  `codigo_asignar_inventario` bigint(20) NOT NULL AUTO_INCREMENT,
  `cantidad_existente` bigint(20) NOT NULL,
  `stock` bigint(20) NOT NULL,
  `fk_inventario_medicamento` bigint(20) NOT NULL,
  `fk_medicamento` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_asignar_inventario`),
  KEY `fk_inventario_medicamento` (`fk_inventario_medicamento`),
  KEY `fk_medicamento` (`fk_medicamento`),
  CONSTRAINT `asignar_inventario_ibfk_1` FOREIGN KEY (`fk_inventario_medicamento`) REFERENCES `inventario_medicamento` (`codigo_inventario`),
  CONSTRAINT `asignar_inventario_ibfk_2` FOREIGN KEY (`fk_medicamento`) REFERENCES `medicamento` (`codigo_medicamento`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignar_inventario`
--

LOCK TABLES `asignar_inventario` WRITE;
/*!40000 ALTER TABLE `asignar_inventario` DISABLE KEYS */;
INSERT INTO `asignar_inventario` VALUES (3,5,0,1,1);
/*!40000 ALTER TABLE `asignar_inventario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cita`
--

DROP TABLE IF EXISTS `cita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cita` (
  `codigo_cita` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha_cita` date NOT NULL,
  `hora_cita` time NOT NULL,
  `valor_cita` bigint(20) NOT NULL,
  `fk_operario` bigint(20) NOT NULL,
  `fk_paciente` bigint(20) NOT NULL,
  `fk_medico` bigint(20) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_cita`),
  KEY `fk_operario` (`fk_operario`),
  KEY `fk_paciente` (`fk_paciente`),
  KEY `fk_medico` (`fk_medico`),
  KEY `fk_estado` (`fk_estado`),
  CONSTRAINT `cita_ibfk_1` FOREIGN KEY (`fk_operario`) REFERENCES `operario` (`cedula_operario`),
  CONSTRAINT `cita_ibfk_2` FOREIGN KEY (`fk_paciente`) REFERENCES `paciente` (`cedula_paciente`),
  CONSTRAINT `cita_ibfk_3` FOREIGN KEY (`fk_medico`) REFERENCES `medico` (`cedula_medico`),
  CONSTRAINT `cita_ibfk_4` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cita`
--

LOCK TABLES `cita` WRITE;
/*!40000 ALTER TABLE `cita` DISABLE KEYS */;
INSERT INTO `cita` VALUES (1,'2016-03-15','08:20:00',1500,1030676001,1030676001,1030676001,1),(2,'2016-05-19','08:22:00',1500,1030676001,1030676001,1030676001,2);
/*!40000 ALTER TABLE `cita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_factura_compra`
--

DROP TABLE IF EXISTS `detalle_factura_compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_factura_compra` (
  `codigo_detalle_factura_compra` bigint(20) NOT NULL AUTO_INCREMENT,
  `cantidad_entrada` bigint(20) NOT NULL,
  `subtotal` bigint(20) NOT NULL,
  `fk_factura_compra` bigint(20) NOT NULL,
  `fk_medicamento` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_detalle_factura_compra`),
  KEY `fk_factura_compra` (`fk_factura_compra`),
  KEY `fk_medicamento` (`fk_medicamento`),
  CONSTRAINT `detalle_factura_compra_ibfk_1` FOREIGN KEY (`fk_factura_compra`) REFERENCES `factura_compra` (`codigo_factura_compra`),
  CONSTRAINT `detalle_factura_compra_ibfk_2` FOREIGN KEY (`fk_medicamento`) REFERENCES `medicamento` (`codigo_medicamento`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_factura_compra`
--

LOCK TABLES `detalle_factura_compra` WRITE;
/*!40000 ALTER TABLE `detalle_factura_compra` DISABLE KEYS */;
INSERT INTO `detalle_factura_compra` VALUES (1,10,1500,1,1);
/*!40000 ALTER TABLE `detalle_factura_compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_factura_venta`
--

DROP TABLE IF EXISTS `detalle_factura_venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_factura_venta` (
  `codigo_detalle_factura_venta` bigint(20) NOT NULL AUTO_INCREMENT,
  `cantidad_salida` bigint(20) NOT NULL,
  `subtotal` bigint(20) NOT NULL,
  `fk_factura_venta` bigint(20) NOT NULL,
  `fk_medicamento` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_detalle_factura_venta`),
  KEY `fk_factura_venta` (`fk_factura_venta`),
  KEY `fk_medicamento` (`fk_medicamento`),
  CONSTRAINT `detalle_factura_venta_ibfk_1` FOREIGN KEY (`fk_factura_venta`) REFERENCES `factura_venta` (`codigo_factura_venta`),
  CONSTRAINT `detalle_factura_venta_ibfk_2` FOREIGN KEY (`fk_medicamento`) REFERENCES `medicamento` (`codigo_medicamento`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_factura_venta`
--

LOCK TABLES `detalle_factura_venta` WRITE;
/*!40000 ALTER TABLE `detalle_factura_venta` DISABLE KEYS */;
INSERT INTO `detalle_factura_venta` VALUES (1,10,1500,1,1),(2,5,1500,1,1);
/*!40000 ALTER TABLE `detalle_factura_venta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diagnostico`
--

DROP TABLE IF EXISTS `diagnostico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diagnostico` (
  `codigo_diagnostico` bigint(20) NOT NULL AUTO_INCREMENT,
  `diagnostico_medico` varchar(50) NOT NULL,
  `diagnostico_enfermeria` varchar(50) NOT NULL,
  `fecha_diagnostico` date NOT NULL,
  `fk_medicamento` bigint(20) NOT NULL,
  `fk_medico` bigint(20) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_diagnostico`),
  KEY `fk_medicamento` (`fk_medicamento`),
  KEY `fk_medico` (`fk_medico`),
  KEY `fk_estado` (`fk_estado`),
  CONSTRAINT `diagnostico_ibfk_1` FOREIGN KEY (`fk_medicamento`) REFERENCES `medicamento` (`codigo_medicamento`),
  CONSTRAINT `diagnostico_ibfk_2` FOREIGN KEY (`fk_medico`) REFERENCES `medico` (`cedula_medico`),
  CONSTRAINT `diagnostico_ibfk_3` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diagnostico`
--

LOCK TABLES `diagnostico` WRITE;
/*!40000 ALTER TABLE `diagnostico` DISABLE KEYS */;
INSERT INTO `diagnostico` VALUES (3,'enfermo','muy enfermo','0000-00-00',1,1030676001,1),(4,'enfermo','muy enfermo','0000-00-00',1,1030676001,1),(5,'enfermo','muy enfermo','0000-00-00',1,1030676001,1),(6,'enfermo','muy enfermo','2016-05-26',1,1030676001,1);
/*!40000 ALTER TABLE `diagnostico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `codigo_estado` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_estado` varchar(50) NOT NULL,
  PRIMARY KEY (`codigo_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` VALUES (1,'Activo'),(2,'Inactivo'),(3,'Inactivo');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factura_compra`
--

DROP TABLE IF EXISTS `factura_compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factura_compra` (
  `codigo_factura_compra` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha_factura_compra` date NOT NULL,
  `total_compra` bigint(20) NOT NULL,
  `fk_forma_pago` bigint(20) NOT NULL,
  `fk_operario` bigint(20) NOT NULL,
  `fk_proveedor` bigint(20) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_factura_compra`),
  KEY `fk_forma_pago` (`fk_forma_pago`),
  KEY `fk_operario` (`fk_operario`),
  KEY `fk_proveedor` (`fk_proveedor`),
  KEY `fk_estado` (`fk_estado`),
  CONSTRAINT `factura_compra_ibfk_1` FOREIGN KEY (`fk_forma_pago`) REFERENCES `forma_pago` (`codigo_forma_pago`),
  CONSTRAINT `factura_compra_ibfk_2` FOREIGN KEY (`fk_operario`) REFERENCES `operario` (`cedula_operario`),
  CONSTRAINT `factura_compra_ibfk_3` FOREIGN KEY (`fk_proveedor`) REFERENCES `proveedor` (`cedula_proveedor`),
  CONSTRAINT `factura_compra_ibfk_4` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura_compra`
--

LOCK TABLES `factura_compra` WRITE;
/*!40000 ALTER TABLE `factura_compra` DISABLE KEYS */;
INSERT INTO `factura_compra` VALUES (1,'2016-05-19',1500,1,1030676001,1030676001,1),(2,'2010-05-20',15000,1,1030676001,1030676001,1),(3,'2016-05-20',15000,1,1030676001,1030676001,1);
/*!40000 ALTER TABLE `factura_compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factura_venta`
--

DROP TABLE IF EXISTS `factura_venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factura_venta` (
  `codigo_factura_venta` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha_factura_venta` date NOT NULL,
  `total_venta` bigint(20) NOT NULL,
  `fk_forma_pago` bigint(20) NOT NULL,
  `fk_cita` bigint(20) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_factura_venta`),
  KEY `fk_forma_pago` (`fk_forma_pago`),
  KEY `fk_cita` (`fk_cita`),
  KEY `fk_estado` (`fk_estado`),
  CONSTRAINT `factura_venta_ibfk_1` FOREIGN KEY (`fk_forma_pago`) REFERENCES `forma_pago` (`codigo_forma_pago`),
  CONSTRAINT `factura_venta_ibfk_2` FOREIGN KEY (`fk_cita`) REFERENCES `cita` (`codigo_cita`),
  CONSTRAINT `factura_venta_ibfk_3` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura_venta`
--

LOCK TABLES `factura_venta` WRITE;
/*!40000 ALTER TABLE `factura_venta` DISABLE KEYS */;
INSERT INTO `factura_venta` VALUES (1,'2016-05-19',1500,1,1,1),(2,'2016-05-26',15000,1,1,1),(3,'2010-05-27',5200,1,1,1);
/*!40000 ALTER TABLE `factura_venta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forma_pago`
--

DROP TABLE IF EXISTS `forma_pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forma_pago` (
  `codigo_forma_pago` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_forma_pago` varchar(50) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_forma_pago`),
  KEY `fk_estado` (`fk_estado`),
  CONSTRAINT `forma_pago_ibfk_1` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forma_pago`
--

LOCK TABLES `forma_pago` WRITE;
/*!40000 ALTER TABLE `forma_pago` DISABLE KEYS */;
INSERT INTO `forma_pago` VALUES (1,'Credito',1),(2,'Debito',1);
/*!40000 ALTER TABLE `forma_pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historial`
--

DROP TABLE IF EXISTS `historial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historial` (
  `codigo_historial` bigint(20) NOT NULL AUTO_INCREMENT,
  `antecedentes` varchar(50) NOT NULL,
  `procedimientoActual` varchar(50) NOT NULL,
  `alergias` varchar(50) NOT NULL,
  `fk_medico` bigint(20) NOT NULL,
  `fk_paciente` bigint(20) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_historial`),
  KEY `fk_medico` (`fk_medico`),
  KEY `fk_paciente` (`fk_paciente`),
  KEY `fk_estado` (`fk_estado`),
  CONSTRAINT `historial_ibfk_1` FOREIGN KEY (`fk_medico`) REFERENCES `medico` (`cedula_medico`),
  CONSTRAINT `historial_ibfk_2` FOREIGN KEY (`fk_paciente`) REFERENCES `paciente` (`cedula_paciente`),
  CONSTRAINT `historial_ibfk_3` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historial`
--

LOCK TABLES `historial` WRITE;
/*!40000 ALTER TABLE `historial` DISABLE KEYS */;
INSERT INTO `historial` VALUES (1,'Ninguno','Dolor de cabeza','Ninguna',1030676001,1030676001,1);
/*!40000 ALTER TABLE `historial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventario_medicamento`
--

DROP TABLE IF EXISTS `inventario_medicamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventario_medicamento` (
  `codigo_inventario` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_inventario` varchar(50) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_inventario`),
  KEY `fk_estado` (`fk_estado`),
  CONSTRAINT `inventario_medicamento_ibfk_1` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventario_medicamento`
--

LOCK TABLES `inventario_medicamento` WRITE;
/*!40000 ALTER TABLE `inventario_medicamento` DISABLE KEYS */;
INSERT INTO `inventario_medicamento` VALUES (1,'Paracetamol',1);
/*!40000 ALTER TABLE `inventario_medicamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicamento`
--

DROP TABLE IF EXISTS `medicamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medicamento` (
  `codigo_medicamento` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_medicamento` varchar(50) NOT NULL,
  `descripcion_medicamento` varchar(50) NOT NULL,
  `contraindicaciones_medicamento` varchar(50) NOT NULL,
  `sigla_medicamento` varchar(50) DEFAULT NULL,
  `fecha_elaboracion` date NOT NULL,
  `fecha_vencimiento` date NOT NULL,
  `valor_medicamento` bigint(20) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_medicamento`),
  KEY `fk_estado` (`fk_estado`),
  CONSTRAINT `medicamento_ibfk_1` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicamento`
--

LOCK TABLES `medicamento` WRITE;
/*!40000 ALTER TABLE `medicamento` DISABLE KEYS */;
INSERT INTO `medicamento` VALUES (1,'paracetamol','100% paracetamol','Alergia','PAR','0000-00-00','0000-00-00',600,1),(2,'plasil','100% paracetamol','Alergia','PAR','1997-02-15','2018-03-03',600,1);
/*!40000 ALTER TABLE `medicamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medico`
--

DROP TABLE IF EXISTS `medico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medico` (
  `cedula_medico` bigint(20) NOT NULL,
  `nombre_medico` varchar(50) NOT NULL,
  `apellido_medico` varchar(50) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `correo_medico` varchar(50) DEFAULT NULL,
  `telefono_medico` bigint(20) NOT NULL,
  `direccion_medico` varchar(50) NOT NULL,
  `genero_medico` varchar(50) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`cedula_medico`),
  KEY `fk_estado` (`fk_estado`),
  CONSTRAINT `medico_ibfk_1` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medico`
--

LOCK TABLES `medico` WRITE;
/*!40000 ALTER TABLE `medico` DISABLE KEYS */;
INSERT INTO `medico` VALUES (1030676001,'Pepe','Lula','1997-03-27','`pepelola@hotmail.com',4412322,'cll 1 no 3a4','M',1),(1030676002,'Pablo','Lula','1997-03-27','`pepelola@hotmail.com',4412322,'cll 1 no 3a4','M',1);
/*!40000 ALTER TABLE `medico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operario`
--

DROP TABLE IF EXISTS `operario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operario` (
  `cedula_operario` bigint(20) NOT NULL,
  `nombre_operario` varchar(50) NOT NULL,
  `apellido_operario` varchar(50) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `correo_operario` varchar(50) DEFAULT NULL,
  `telefono_operario` bigint(20) NOT NULL,
  `direccion_operario` varchar(50) NOT NULL,
  `genero_operario` varchar(50) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`cedula_operario`),
  KEY `fk_estado` (`fk_estado`),
  CONSTRAINT `operario_ibfk_1` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operario`
--

LOCK TABLES `operario` WRITE;
/*!40000 ALTER TABLE `operario` DISABLE KEYS */;
INSERT INTO `operario` VALUES (1030676001,'Juan','Lula','1997-03-27','juanlola@hotmail.com',4412322,'cll 1 no 3a4','M',1);
/*!40000 ALTER TABLE `operario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paciente`
--

DROP TABLE IF EXISTS `paciente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paciente` (
  `cedula_paciente` bigint(20) NOT NULL,
  `nombre_paciente` varchar(50) NOT NULL,
  `apellido_paciente` varchar(50) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `correo_paciente` varchar(50) DEFAULT NULL,
  `telefono_paciente` bigint(20) NOT NULL,
  `direccion_paciente` varchar(50) NOT NULL,
  `genero_paciente` varchar(50) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`cedula_paciente`),
  KEY `fk_estado` (`fk_estado`),
  CONSTRAINT `paciente_ibfk_1` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paciente`
--

LOCK TABLES `paciente` WRITE;
/*!40000 ALTER TABLE `paciente` DISABLE KEYS */;
INSERT INTO `paciente` VALUES (1030676001,'Pepe','Lula','1997-03-27','`pepelola@hotmail.com',4412322,'cll 1 no 3a4','M',1);
/*!40000 ALTER TABLE `paciente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor` (
  `cedula_proveedor` bigint(20) NOT NULL,
  `nombre_proveedor` varchar(50) NOT NULL,
  `apellido_proveedor` varchar(50) NOT NULL,
  `correo_proveedor` varchar(50) DEFAULT NULL,
  `telefono_proveedor` bigint(20) NOT NULL,
  `genero_proveedor` varchar(50) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`cedula_proveedor`),
  KEY `fk_estado` (`fk_estado`),
  CONSTRAINT `proveedor_ibfk_1` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
INSERT INTO `proveedor` VALUES (1030676001,'Pepe','Lula','pepelola@hotmail.com',44123,'M',1);
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `vasignar_iventario`
--

DROP TABLE IF EXISTS `vasignar_iventario`;
/*!50001 DROP VIEW IF EXISTS `vasignar_iventario`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vasignar_iventario` AS SELECT 
 1 AS `CODIGO`,
 1 AS `CANTIDAD`,
 1 AS `STOCK`,
 1 AS `INVENTARIO`,
 1 AS `MEDICAMENTO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vcita`
--

DROP TABLE IF EXISTS `vcita`;
/*!50001 DROP VIEW IF EXISTS `vcita`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vcita` AS SELECT 
 1 AS `CODIGO`,
 1 AS `FECHA`,
 1 AS `HORA`,
 1 AS `VALOR`,
 1 AS `OPERARIO`,
 1 AS `PACIENTE`,
 1 AS `MEDICO`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vcitaactivo`
--

DROP TABLE IF EXISTS `vcitaactivo`;
/*!50001 DROP VIEW IF EXISTS `vcitaactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vcitaactivo` AS SELECT 
 1 AS `CODIGO`,
 1 AS `FECHA`,
 1 AS `HORA`,
 1 AS `VALOR`,
 1 AS `OPERARIO`,
 1 AS `PACIENTE`,
 1 AS `MEDICO`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vcitainactivo`
--

DROP TABLE IF EXISTS `vcitainactivo`;
/*!50001 DROP VIEW IF EXISTS `vcitainactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vcitainactivo` AS SELECT 
 1 AS `CODIGO`,
 1 AS `FECHA`,
 1 AS `HORA`,
 1 AS `VALOR`,
 1 AS `OPERARIO`,
 1 AS `PACIENTE`,
 1 AS `MEDICO`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vdetalle_factura_compra`
--

DROP TABLE IF EXISTS `vdetalle_factura_compra`;
/*!50001 DROP VIEW IF EXISTS `vdetalle_factura_compra`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vdetalle_factura_compra` AS SELECT 
 1 AS `CODIGO`,
 1 AS `CANTIDAD`,
 1 AS `SUBTOTAL`,
 1 AS `NOFACTURA`,
 1 AS `MEDICAMENTO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vdetalle_factura_venta`
--

DROP TABLE IF EXISTS `vdetalle_factura_venta`;
/*!50001 DROP VIEW IF EXISTS `vdetalle_factura_venta`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vdetalle_factura_venta` AS SELECT 
 1 AS `CODIGO`,
 1 AS `CANTIDAD`,
 1 AS `SUBTOTAL`,
 1 AS `NOFACTURA`,
 1 AS `MEDICAMENTO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vdiagnostico`
--

DROP TABLE IF EXISTS `vdiagnostico`;
/*!50001 DROP VIEW IF EXISTS `vdiagnostico`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vdiagnostico` AS SELECT 
 1 AS `CODIGO`,
 1 AS `DIAGNOSTICO`,
 1 AS `DESCRIPCION`,
 1 AS `FECHA`,
 1 AS `MEDICAMENTO`,
 1 AS `MEDICO`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vdiagnosticoactivo`
--

DROP TABLE IF EXISTS `vdiagnosticoactivo`;
/*!50001 DROP VIEW IF EXISTS `vdiagnosticoactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vdiagnosticoactivo` AS SELECT 
 1 AS `CODIGO`,
 1 AS `DIAGNOSTICO`,
 1 AS `DESCRIPCION`,
 1 AS `FECHA`,
 1 AS `MEDICAMENTO`,
 1 AS `MEDICO`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vdiagnosticoinactivo`
--

DROP TABLE IF EXISTS `vdiagnosticoinactivo`;
/*!50001 DROP VIEW IF EXISTS `vdiagnosticoinactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vdiagnosticoinactivo` AS SELECT 
 1 AS `CODIGO`,
 1 AS `DIAGNOSTICO`,
 1 AS `DESCRIPCION`,
 1 AS `FECHA`,
 1 AS `MEDICAMENTO`,
 1 AS `MEDICO`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vestado`
--

DROP TABLE IF EXISTS `vestado`;
/*!50001 DROP VIEW IF EXISTS `vestado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vestado` AS SELECT 
 1 AS `CODIGO`,
 1 AS `NOMBRE`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vfactura_compra`
--

DROP TABLE IF EXISTS `vfactura_compra`;
/*!50001 DROP VIEW IF EXISTS `vfactura_compra`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vfactura_compra` AS SELECT 
 1 AS `CODIGO`,
 1 AS `FECHACOMPRA`,
 1 AS `TOTAL`,
 1 AS `FORMAPAGO`,
 1 AS `OPERARIO`,
 1 AS `PROVEEDOR`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vfactura_compraactivo`
--

DROP TABLE IF EXISTS `vfactura_compraactivo`;
/*!50001 DROP VIEW IF EXISTS `vfactura_compraactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vfactura_compraactivo` AS SELECT 
 1 AS `CODIGO`,
 1 AS `FECHACOMPRA`,
 1 AS `TOTAL`,
 1 AS `FORMAPAGO`,
 1 AS `OPERARIO`,
 1 AS `PROVEEDOR`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vfactura_comprainactivo`
--

DROP TABLE IF EXISTS `vfactura_comprainactivo`;
/*!50001 DROP VIEW IF EXISTS `vfactura_comprainactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vfactura_comprainactivo` AS SELECT 
 1 AS `CODIGO`,
 1 AS `FECHACOMPRA`,
 1 AS `TOTAL`,
 1 AS `FORMAPAGO`,
 1 AS `OPERARIO`,
 1 AS `PROVEEDOR`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vfactura_venta`
--

DROP TABLE IF EXISTS `vfactura_venta`;
/*!50001 DROP VIEW IF EXISTS `vfactura_venta`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vfactura_venta` AS SELECT 
 1 AS `CODIGO`,
 1 AS `FECHAVENTA`,
 1 AS `TOTAL`,
 1 AS `FORMAPAGO`,
 1 AS `NOCITA`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vfactura_ventaactivo`
--

DROP TABLE IF EXISTS `vfactura_ventaactivo`;
/*!50001 DROP VIEW IF EXISTS `vfactura_ventaactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vfactura_ventaactivo` AS SELECT 
 1 AS `CODIGO`,
 1 AS `FECHAVENTA`,
 1 AS `TOTAL`,
 1 AS `FORMAPAGO`,
 1 AS `NOCITA`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vfactura_ventainactivo`
--

DROP TABLE IF EXISTS `vfactura_ventainactivo`;
/*!50001 DROP VIEW IF EXISTS `vfactura_ventainactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vfactura_ventainactivo` AS SELECT 
 1 AS `CODIGO`,
 1 AS `FECHAVENTA`,
 1 AS `TOTAL`,
 1 AS `FORMAPAGO`,
 1 AS `NOCITA`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vforma_pago`
--

DROP TABLE IF EXISTS `vforma_pago`;
/*!50001 DROP VIEW IF EXISTS `vforma_pago`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vforma_pago` AS SELECT 
 1 AS `CODIGO`,
 1 AS `NOMBRE`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vforma_pagoactivo`
--

DROP TABLE IF EXISTS `vforma_pagoactivo`;
/*!50001 DROP VIEW IF EXISTS `vforma_pagoactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vforma_pagoactivo` AS SELECT 
 1 AS `CODIGO`,
 1 AS `NOMBRE`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vforma_pagoinactivo`
--

DROP TABLE IF EXISTS `vforma_pagoinactivo`;
/*!50001 DROP VIEW IF EXISTS `vforma_pagoinactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vforma_pagoinactivo` AS SELECT 
 1 AS `CODIGO`,
 1 AS `NOMBRE`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vhistorial`
--

DROP TABLE IF EXISTS `vhistorial`;
/*!50001 DROP VIEW IF EXISTS `vhistorial`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vhistorial` AS SELECT 
 1 AS `CODIGO`,
 1 AS `ANTECEDENTES`,
 1 AS `PROCEDIMIENTO`,
 1 AS `ALERGIAS`,
 1 AS `MEDICO`,
 1 AS `PACIENTE`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vhistorialactivo`
--

DROP TABLE IF EXISTS `vhistorialactivo`;
/*!50001 DROP VIEW IF EXISTS `vhistorialactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vhistorialactivo` AS SELECT 
 1 AS `CODIGO`,
 1 AS `ANTECEDENTES`,
 1 AS `PROCEDIMIENTO`,
 1 AS `ALERGIAS`,
 1 AS `MEDICO`,
 1 AS `PACIENTE`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vhistorialinactivo`
--

DROP TABLE IF EXISTS `vhistorialinactivo`;
/*!50001 DROP VIEW IF EXISTS `vhistorialinactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vhistorialinactivo` AS SELECT 
 1 AS `CODIGO`,
 1 AS `ANTECEDENTES`,
 1 AS `PROCEDIMIENTO`,
 1 AS `ALERGIAS`,
 1 AS `MEDICO`,
 1 AS `PACIENTE`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vinventario_medicamento`
--

DROP TABLE IF EXISTS `vinventario_medicamento`;
/*!50001 DROP VIEW IF EXISTS `vinventario_medicamento`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vinventario_medicamento` AS SELECT 
 1 AS `CODIGO`,
 1 AS `NOMBRE`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vinventario_medicamentoactivo`
--

DROP TABLE IF EXISTS `vinventario_medicamentoactivo`;
/*!50001 DROP VIEW IF EXISTS `vinventario_medicamentoactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vinventario_medicamentoactivo` AS SELECT 
 1 AS `CODIGO`,
 1 AS `NOMBRE`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vinventario_medicamentoinactivo`
--

DROP TABLE IF EXISTS `vinventario_medicamentoinactivo`;
/*!50001 DROP VIEW IF EXISTS `vinventario_medicamentoinactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vinventario_medicamentoinactivo` AS SELECT 
 1 AS `CODIGO`,
 1 AS `NOMBRE`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vmedicamento`
--

DROP TABLE IF EXISTS `vmedicamento`;
/*!50001 DROP VIEW IF EXISTS `vmedicamento`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vmedicamento` AS SELECT 
 1 AS `CODIGO`,
 1 AS `NOMBRE`,
 1 AS `DESCRIPCION`,
 1 AS `CONTRAINDICACIONES`,
 1 AS `SIGLA`,
 1 AS `ELABORADO`,
 1 AS `VENCE`,
 1 AS `VALOR`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vmedicamentoactivo`
--

DROP TABLE IF EXISTS `vmedicamentoactivo`;
/*!50001 DROP VIEW IF EXISTS `vmedicamentoactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vmedicamentoactivo` AS SELECT 
 1 AS `CODIGO`,
 1 AS `NOMBRE`,
 1 AS `DESCRIPCION`,
 1 AS `CONTRAINDICACIONES`,
 1 AS `SIGLA`,
 1 AS `ELABORADO`,
 1 AS `VENCE`,
 1 AS `VALOR`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vmedicamentoinactivo`
--

DROP TABLE IF EXISTS `vmedicamentoinactivo`;
/*!50001 DROP VIEW IF EXISTS `vmedicamentoinactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vmedicamentoinactivo` AS SELECT 
 1 AS `CODIGO`,
 1 AS `NOMBRE`,
 1 AS `DESCRIPCION`,
 1 AS `CONTRAINDICACIONES`,
 1 AS `SIGLA`,
 1 AS `ELABORADO`,
 1 AS `VENCE`,
 1 AS `VALOR`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vmedico`
--

DROP TABLE IF EXISTS `vmedico`;
/*!50001 DROP VIEW IF EXISTS `vmedico`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vmedico` AS SELECT 
 1 AS `CEDULA`,
 1 AS `NOMBRE`,
 1 AS `APELLIDO`,
 1 AS `NACIMIENTO`,
 1 AS `CORREO`,
 1 AS `TELEFONO`,
 1 AS `DIRECCION`,
 1 AS `MEDICO`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vmedicoactivo`
--

DROP TABLE IF EXISTS `vmedicoactivo`;
/*!50001 DROP VIEW IF EXISTS `vmedicoactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vmedicoactivo` AS SELECT 
 1 AS `CEDULA`,
 1 AS `NOMBRE`,
 1 AS `APELLIDO`,
 1 AS `NACIMIENTO`,
 1 AS `CORREO`,
 1 AS `TELEFONO`,
 1 AS `DIRECCION`,
 1 AS `MEDICO`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vmedicoinactivo`
--

DROP TABLE IF EXISTS `vmedicoinactivo`;
/*!50001 DROP VIEW IF EXISTS `vmedicoinactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vmedicoinactivo` AS SELECT 
 1 AS `CEDULA`,
 1 AS `NOMBRE`,
 1 AS `APELLIDO`,
 1 AS `NACIMIENTO`,
 1 AS `CORREO`,
 1 AS `TELEFONO`,
 1 AS `DIRECCION`,
 1 AS `MEDICO`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `voperario`
--

DROP TABLE IF EXISTS `voperario`;
/*!50001 DROP VIEW IF EXISTS `voperario`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `voperario` AS SELECT 
 1 AS `CEDULA`,
 1 AS `NOMBRE`,
 1 AS `APELLIDO`,
 1 AS `NACIMIENTO`,
 1 AS `CORREO`,
 1 AS `TELEFONO`,
 1 AS `DIRECCION`,
 1 AS `OPERARIO`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `voperarioactivo`
--

DROP TABLE IF EXISTS `voperarioactivo`;
/*!50001 DROP VIEW IF EXISTS `voperarioactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `voperarioactivo` AS SELECT 
 1 AS `CEDULA`,
 1 AS `NOMBRE`,
 1 AS `APELLIDO`,
 1 AS `NACIMIENTO`,
 1 AS `CORREO`,
 1 AS `TELEFONO`,
 1 AS `DIRECCION`,
 1 AS `OPERARIO`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `voperarioinactivo`
--

DROP TABLE IF EXISTS `voperarioinactivo`;
/*!50001 DROP VIEW IF EXISTS `voperarioinactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `voperarioinactivo` AS SELECT 
 1 AS `CEDULA`,
 1 AS `NOMBRE`,
 1 AS `APELLIDO`,
 1 AS `NACIMIENTO`,
 1 AS `CORREO`,
 1 AS `TELEFONO`,
 1 AS `DIRECCION`,
 1 AS `OPERARIO`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vpaciente`
--

DROP TABLE IF EXISTS `vpaciente`;
/*!50001 DROP VIEW IF EXISTS `vpaciente`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vpaciente` AS SELECT 
 1 AS `CEDULA`,
 1 AS `NOMBRE`,
 1 AS `APELLIDO`,
 1 AS `NACIMIENTO`,
 1 AS `CORREO`,
 1 AS `TELEFONO`,
 1 AS `DIRECCION`,
 1 AS `PACIENTE`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vpacienteactivo`
--

DROP TABLE IF EXISTS `vpacienteactivo`;
/*!50001 DROP VIEW IF EXISTS `vpacienteactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vpacienteactivo` AS SELECT 
 1 AS `CEDULA`,
 1 AS `NOMBRE`,
 1 AS `APELLIDO`,
 1 AS `NACIMIENTO`,
 1 AS `CORREO`,
 1 AS `TELEFONO`,
 1 AS `DIRECCION`,
 1 AS `OPERARIO`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vpacienteinactivo`
--

DROP TABLE IF EXISTS `vpacienteinactivo`;
/*!50001 DROP VIEW IF EXISTS `vpacienteinactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vpacienteinactivo` AS SELECT 
 1 AS `CEDULA`,
 1 AS `NOMBRE`,
 1 AS `APELLIDO`,
 1 AS `NACIMIENTO`,
 1 AS `CORREO`,
 1 AS `TELEFONO`,
 1 AS `DIRECCION`,
 1 AS `OPERARIO`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vproveedor`
--

DROP TABLE IF EXISTS `vproveedor`;
/*!50001 DROP VIEW IF EXISTS `vproveedor`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vproveedor` AS SELECT 
 1 AS `CEDULA`,
 1 AS `NOMBRE`,
 1 AS `APELLIDO`,
 1 AS `CORREO`,
 1 AS `TELEFONO`,
 1 AS `PROVEEDOR`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vproveedoractivo`
--

DROP TABLE IF EXISTS `vproveedoractivo`;
/*!50001 DROP VIEW IF EXISTS `vproveedoractivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vproveedoractivo` AS SELECT 
 1 AS `CEDULA`,
 1 AS `NOMBRE`,
 1 AS `APELLIDO`,
 1 AS `CORREO`,
 1 AS `TELEFONO`,
 1 AS `PROVEEDOR`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vproveedorinactivo`
--

DROP TABLE IF EXISTS `vproveedorinactivo`;
/*!50001 DROP VIEW IF EXISTS `vproveedorinactivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vproveedorinactivo` AS SELECT 
 1 AS `CEDULA`,
 1 AS `NOMBRE`,
 1 AS `APELLIDO`,
 1 AS `CORREO`,
 1 AS `TELEFONO`,
 1 AS `PROVEEDOR`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'consultorio'
--
/*!50003 DROP PROCEDURE IF EXISTS `AddAsignarInventario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddAsignarInventario`(in codigo_asignar_inventarioP bigint, 
in cantidad_existenteP bigint, in stockP bigint, in fk_inventario_medicamentoP bigint,
in fk_medicamentoP bigint)
begin insert into asignar_inventario
(codigo_asignar_inventario, cantidad_existente, stock, fk_inventario_medicamento, fk_medicamento) 
values(codigo_asignar_inventarioP, cantidad_existenteP, stockP, fk_inventario_medicamentoP, fk_medicamentoP); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddCita` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddCita`(in codigo_citaP bigint, 
in fecha_citaP date, in hora_citaP time, in valor_citaP bigint,
in fk_operarioP bigint, in fk_pacienteP bigint, in fk_medicoP bigint,
in fk_estadoP bigint)
begin insert into cita
(codigo_cita, fecha_cita, hora_cita, valor_cita ,
fk_operario , fk_paciente , fk_medico ,
fk_estado )
values(codigo_citaP, fecha_citaP, hora_citaP, valor_citaP ,
fk_operarioP , fk_pacienteP , fk_medicoP ,
fk_estadoP); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddDetalleFacturaCompra` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddDetalleFacturaCompra`(in codigo_detalle_factura_compraP bigint, 
in cantidad_entradaP bigint, in subtotalP bigint, in fk_factura_compraP bigint,
in fk_medicamentoP bigint)
begin insert into detalle_factura_compra
(codigo_detalle_factura_compra,
cantidad_entrada, subtotal, fk_factura_compra,
fk_medicamento)
values(codigo_detalle_factura_compraP,
cantidad_entradaP, subtotalP, fk_factura_compraP,
fk_medicamentoP); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddDetalleFacturaVenta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddDetalleFacturaVenta`(in codigo_detalle_factura_ventaP bigint, 
in cantidad_salidaP bigint, in subtotalP bigint, in fk_factura_ventaP bigint,
in fk_medicamentoP bigint)
begin insert into detalle_factura_venta
(codigo_detalle_factura_venta,
cantidad_salida, subtotal, fk_factura_venta,
fk_medicamento)
values(codigo_detalle_factura_ventaP,
cantidad_salidaP, subtotalP, fk_factura_ventaP,
fk_medicamentoP); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddDiagnostico` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddDiagnostico`(in codigo_diagnosticoP bigint, 
in diagnostico_medicoP varchar(50), in diagnostico_enfermeriaP varchar(50), 
in fecha_diagnosticoP date, in fk_medicamentoP bigint,
in fk_medicoP bigint, in fk_estadoP bigint)
begin insert into diagnostico
(codigo_diagnostico, 
diagnostico_medico, diagnostico_enfermeria , 
fecha_diagnostico, fk_medicamento ,
fk_medico , fk_estado)
values(codigo_diagnosticoP, 
diagnostico_medicoP, diagnostico_enfermeriaP , 
fecha_diagnosticoP, fk_medicamentoP ,
fk_medicoP , fk_estadoP); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddEstado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddEstado`(in codigo_estadoP bigint, 
in nombre_estadoP varchar(50))
begin insert into estado
(codigo_estado, nombre_estado)
values(codigo_estadoP, nombre_estadoP); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddFacturaCompra` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddFacturaCompra`(in codigo_factura_compraP bigint, 
in fecha_factura_compraP date, in total_compraP bigint, 
in fk_forma_pagoP bigint, in fk_operarioP bigint,
in fk_proveedorP bigint, in fk_estadoP bigint)
begin insert into factura_compra
(codigo_factura_compra, 
fecha_factura_compra ,  total_compra ,
fk_forma_pago , fk_operario ,
fk_proveedor ,  fk_estado )
values(codigo_factura_compraP, 
fecha_factura_compraP ,  total_compraP ,
fk_forma_pagoP , fk_operarioP ,
fk_proveedorP ,  fk_estadoP); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddFacturaVenta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddFacturaVenta`(in codigo_factura_ventaP bigint, 
in fecha_factura_ventaP date, in total_ventaP bigint, 
in fk_forma_pagoP bigint, in fk_citaP bigint, in fk_estadoP bigint)
begin insert into factura_venta
(codigo_factura_venta ,
fecha_factura_venta , total_venta ,
fk_forma_pago , fk_cita, fk_estado )
values(codigo_factura_ventaP ,
fecha_factura_ventaP , total_ventaP ,
fk_forma_pagoP , fk_citaP , fk_estadoP ); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddFormaPago` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddFormaPago`(in codigo_forma_pagoP bigint, 
in nombre_forma_pagoP varchar(50), in fk_estadoP bigint )
begin insert into forma_pago
(codigo_forma_pago ,
nombre_forma_pago ,  fk_estado )
values(codigo_forma_pagoP ,
nombre_forma_pagoP ,  fk_estadoP); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddHistorial` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddHistorial`(in codigo_historialP bigint, 
in antecedentesP varchar(50), in procedimientoActualP varchar(50),
in alergiasP varchar(50), in fk_medicoP bigint, 
in fk_pacienteP bigint, in fk_estadoP bigint )
begin insert into historial
(codigo_historial ,
antecedentes ,  procedimientoActual ,
alergias , fk_medico , 
fk_paciente , fk_estado )
values(codigo_historialP ,
antecedentesP ,  procedimientoActualP ,
alergiasP , fk_medicoP , 
fk_pacienteP , fk_estadoP); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddInventarioMedicamento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddInventarioMedicamento`(in codigo_inventarioP bigint, 
in nombre_inventarioP varchar(50), in fk_estadoP bigint )
begin insert into inventario_medicamento
(codigo_inventario ,
nombre_inventario ,  fk_estado )
values(codigo_inventarioP ,
nombre_inventarioP ,  fk_estadoP); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddMedicamento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddMedicamento`(in codigo_medicamentoP bigint, 
in nombre_medicamentoP varchar(50), in descripcion_medicamentoP varchar(50),
in contraindicaciones_medicamentoP varchar(50), in sigla_medicamentoP varchar(50), 
in fecha_elaboracionP date, in fecha_vencimientoP date,
in valor_medicamentoP bigint, in fk_estadoP bigint )
begin insert into medicamento
(codigo_medicamento ,
 nombre_medicamento ,  descripcion_medicamento ,
 contraindicaciones_medicamento ,  sigla_medicamento , 
 fecha_elaboracion , fecha_vencimiento ,
 valor_medicamento,  fk_estado )
values(codigo_medicamentoP ,
 nombre_medicamentoP ,  descripcion_medicamentoP ,
 contraindicaciones_medicamentoP ,  sigla_medicamentoP , 
 fecha_elaboracionP , fecha_vencimientoP ,
 valor_medicamentoP ,  fk_estadoP); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddMedico` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddMedico`(in cedula_medicoP bigint, 
in nombre_medicoP varchar(50), in apellido_medicoP varchar(50), 
in fecha_nacimientoP date, in correo_medicoP varchar(50), 
in telefono_medicoP bigint, in direccion_medicoP varchar(50),
in genero_medicoP varchar(50), in fk_estadoP bigint )
begin insert into medico
(cedula_medico, 
 nombre_medico,  apellido_medico , 
 fecha_nacimiento ,  correo_medico , 
 telefono_medico , direccion_medico ,
 genero_medico ,  fk_estado  )
values(cedula_medicoP , 
 nombre_medicoP ,  apellido_medicoP , 
 fecha_nacimientoP ,  correo_medicoP , 
 telefono_medicoP , direccion_medicoP ,
 genero_medicoP ,  fk_estadoP ); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddOperario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddOperario`(in cedula_operarioP bigint, 
in nombre_operarioP varchar(50), in apellido_operarioP varchar(50), 
in fecha_nacimientoP date, in correo_operarioP varchar(50), 
in telefono_operarioP bigint, in direccion_operarioP varchar(50),
in genero_operarioP varchar(50), in fk_estadoP bigint )
begin insert into operario
(cedula_operario, 
 nombre_operario,  apellido_operario , 
 fecha_nacimiento ,  correo_operario , 
 telefono_operario , direccion_operario ,
 genero_operario ,  fk_estado  )
values(cedula_operarioP , 
 nombre_operarioP ,  apellido_operarioP , 
 fecha_nacimientoP ,  correo_operarioP , 
 telefono_operarioP , direccion_operarioP ,
 genero_operarioP ,  fk_estadoP ); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddPaciente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddPaciente`(in cedula_pacienteP bigint, 
in nombre_pacienteP varchar(50), in apellido_pacienteP varchar(50), 
in fecha_nacimientoP date, in correo_pacienteP varchar(50), 
in telefono_pacienteP bigint, in direccion_pacienteP varchar(50),
in genero_pacienteP varchar(50), in fk_estadoP bigint )
begin insert into paciente
(cedula_paciente, 
 nombre_paciente,  apellido_paciente , 
 fecha_nacimiento ,  correo_paciente , 
 telefono_paciente , direccion_paciente ,
 genero_paciente ,  fk_estado  )
values(cedula_pacienteP , 
 nombre_pacienteP ,  apellido_pacienteP , 
 fecha_nacimientoP ,  correo_pacienteP , 
 telefono_pacienteP , direccion_pacienteP ,
 genero_pacienteP ,  fk_estadoP ); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddProveedor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddProveedor`(in cedula_proveedorP bigint, 
in nombre_proveedorP varchar(50), in apellido_proveedorP varchar(50), 
in correo_proveedorP varchar(50), 
in telefono_proveedorP bigint, 
in genero_proveedorP varchar(50), in fk_estadoP bigint )
begin insert into proveedor
(cedula_proveedor, 
 nombre_proveedor ,  apellido_proveedor , 
 correo_proveedor , 
 telefono_proveedor , 
 genero_proveedor ,  fk_estado  )
values(cedula_proveedorP, 
 nombre_proveedorP ,  apellido_proveedorP , 
 correo_proveedorP , 
 telefono_proveedorP , 
 genero_proveedorP ,  fk_estadoP ); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarAsignarInventario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarAsignarInventario`(in codigo_asignar_inventarioP bigint)
begin select * from asignar_inventario
where codigo_asignar_inventario = codigo_asignar_inventarioP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarCita` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarCita`(in codigo_citaP bigint)
begin select * from  cita
where codigo_cita=codigo_citaP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarCitaEstado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarCitaEstado`(in nombre_estadoP varchar(50))
begin 
select
cita.`codigo_cita` AS CODIGO,
cita.`fecha_cita` AS FECHA,
cita.`hora_cita` AS HORA,
cita.`valor_cita` AS VALOR,
operario.nombre_operario AS OPERARIO,
paciente.nombre_paciente AS PACIENTE,
medico.nombre_medico AS MEDICO,
estado.nombre_estado AS ESTADO  
from   cita,estado,medico,paciente,operario
where cita.`fk_operario` = operario.cedula_operario AND
cita.`fk_paciente` = paciente.cedula_paciente AND
cita.`fk_medico` = medico.cedula_medico AND 
cita.`fk_estado` = estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
 end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarDetalleFacturaCompra` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarDetalleFacturaCompra`(in codigo_detalle_factura_compraP bigint)
begin select * from  detalle_factura_compra
where  codigo_detalle_factura_compra = codigo_detalle_factura_compraP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarDetalleFacturaVenta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarDetalleFacturaVenta`(in codigo_detalle_factura_ventaP bigint)
begin select * from detalle_factura_venta
where codigo_detalle_factura_venta=codigo_detalle_factura_ventaP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarDiagnostico` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarDiagnostico`(in codigo_diagnosticoP bigint)
begin select * from  diagnostico
where codigo_diagnostico = codigo_diagnosticoP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarDiagnosticoEstado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarDiagnosticoEstado`(in nombre_estadoP varchar(50))
begin 
select
diagnostico.`codigo_diagnostico` AS CODIGO,
diagnostico.`diagnostico_medico` AS DIAGNOSTICO,
diagnostico.`diagnostico_enfermeria` AS DESCRIPCION,
diagnostico.`fecha_diagnostico` AS FECHA,
medicamento.nombre_medicamento AS MEDICAMENTO,
medico.nombre_medico AS MEDICO,
estado.nombre_estado AS ESTADO
FROM	
diagnostico,medicamento,medico,estado
WHERE
diagnostico.`fk_medicamento` = medicamento.codigo_medicamento AND
diagnostico.fk_medico = medico.cedula_medico AND
diagnostico.fk_estado = estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
 end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarEstado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarEstado`(in codigo_estadooP bigint)
begin select * from estado
where codigo_estado=codigo_estadoP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarFacturaCompra` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarFacturaCompra`(in codigo_factura_compraP bigint)
begin select * from factura_compra
where codigo_factura_compra=codigo_factura_compraP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarfacturacompraEstado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarfacturacompraEstado`(in nombre_estadoP varchar(50))
begin 
select
 
factura_compra.`codigo_factura_compra` AS CODIGO,
factura_compra.`fecha_factura_compra` AS FECHACOMPRA,
factura_compra.`total_compra` AS TOTAL,
forma_pago.nombre_forma_pago AS FORMAPAGO,
operario.nombre_operario AS OPERARIO,
proveedor.nombre_proveedor AS PROVEEDOR,
estado.nombre_estado AS ESTADO
FROM	
factura_compra,forma_pago,operario,proveedor,estado
WHERE
factura_compra.`fk_forma_pago` = forma_pago.codigo_forma_pago AND
factura_compra.`fk_operario` = operario.cedula_operario AND
factura_compra.`fk_proveedor`= proveedor.cedula_proveedor AND 
factura_compra.`fk_estado`= estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarFacturaVenta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarFacturaVenta`(in codigo_factura_ventaP bigint)
begin select * from factura_venta
where codigo_factura_venta= codigo_factura_ventaP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarfacturaventaEstado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarfacturaventaEstado`(in nombre_estadoP varchar(50))
begin 
select
factura_venta.`codigo_factura_venta` AS CODIGO,
factura_venta.`fecha_factura_venta` AS FECHAVENTA,
factura_venta.`total_venta` AS TOTAL,
forma_pago.nombre_forma_pago AS FORMAPAGO,
cita.codigo_cita AS NOCITA,
estado.nombre_estado AS ESTADO
FROM	
factura_venta,forma_pago,cita,estado
WHERE
factura_venta.`fk_forma_pago` = forma_pago.codigo_forma_pago AND
factura_venta.`fk_cita` = cita.codigo_cita AND 
factura_venta.`fk_estado`= estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarfacturaventaFecha` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarfacturaventaFecha`(in fecha_inicioP date,in fecha_finP date)
begin 
select
factura_venta.`codigo_factura_venta` AS CODIGO,
factura_venta.`fecha_factura_venta` AS FECHAVENTA,
factura_venta.`total_venta` AS TOTAL,
detalle_factura_venta.`cantidad_salida` AS CANTIDAD,
detalle_factura_venta.`subtotal` AS SUBTOTAL,
factura_venta.codigo_factura_venta AS NOFACTURA,
medicamento.nombre_medicamento AS MEDICAMENTO,
forma_pago.nombre_forma_pago AS FORMAPAGO,
cita.codigo_cita AS NO_CITA,
estado.nombre_estado AS ESTADO
FROM	
factura_venta,forma_pago,cita,detalle_factura_venta,medicamento,estado
WHERE
factura_venta.`fk_forma_pago` = forma_pago.codigo_forma_pago AND
factura_venta.`fk_cita` = cita.codigo_cita AND 
factura_venta.`fk_estado`= estado.codigo_estado AND
detalle_factura_venta.codigo_detalle_factura_venta = factura_venta.codigo_factura_venta AND
detalle_factura_venta.fk_medicamento = medicamento.codigo_medicamento AND
factura_venta.fecha_factura_venta >= fecha_inicioP AND
factura_venta.fecha_factura_venta <= fecha_finP ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarFormaPago` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarFormaPago`(in codigo_forma_pagoP bigint)
begin select * from forma_pago
where codigo_forma_pago=codigo_forma_pagoP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarformapagoEstado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarformapagoEstado`(in nombre_estadoP varchar(50))
begin 
select
forma_pago.`codigo_forma_pago` AS CODIGO,
forma_pago.`nombre_forma_pago` AS NOMBRE,
estado.nombre_estado AS ESTADO
FROM	
forma_pago,estado
WHERE
forma_pago.`fk_estado`= estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarHistorial` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarHistorial`(in codigo_historialP bigint)
begin select * from historial
where codigo_historial=codigo_historialP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarhistorialEstado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarhistorialEstado`(in nombre_estadoP varchar(50))
begin 
select
historial.`codigo_historial` AS CODIGO,
historial.antecedentes AS ANTECEDENTES,
historial.procedimientoActual AS PROCEDIMIENTO,
historial.alergias AS ALERGIAS,
medico.nombre_medico AS MEDICO,
paciente.nombre_paciente AS PACIENTE,
estado.nombre_estado AS ESTADO
FROM	
historial,medico,paciente,estado
WHERE
historial.`fk_medico`=  medico.cedula_medico AND
historial.fk_paciente = paciente.cedula_paciente AND
historial.fk_estado = estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarInventarioMedicamento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarInventarioMedicamento`(in codigo_inventarioP bigint)
begin select * from inventario_medicamento
where codigo_inventario=codigo_inventarioP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarinventariomedicamentoEstado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarinventariomedicamentoEstado`(in nombre_estadoP varchar(50))
begin 
select
inventario_medicamento.`codigo_inventario` AS CODIGO,
inventario_medicamento.`nombre_inventario` AS NOMBRE,
estado.nombre_estado AS ESTADO
FROM	
inventario_medicamento,estado
WHERE
inventario_medicamento.fk_estado = estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarMedicamento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarMedicamento`(in codigo_medicamentoP bigint)
begin select * from medicamento
 where codigo_medicamento= codigo_medicamentoP ; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarmedicamentoEstado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarmedicamentoEstado`(in nombre_estadoP varchar(50))
begin 
select
medicamento.`codigo_medicamento` AS CODIGO,
medicamento.`nombre_medicamento` AS NOMBRE,
medicamento.`descripcion_medicamento` AS DESCRIPCION,
medicamento.`contraindicaciones_medicamento` AS CONTRAINDICACIONES,
medicamento.`sigla_medicamento`AS SIGLA,
medicamento.`fecha_elaboracion` AS ELABORADO,
medicamento.`fecha_vencimiento` AS VENCE,
medicamento.`valor_medicamento` AS VALOR,
estado.nombre_estado AS ESTADO
FROM	
medicamento,estado
WHERE
medicamento.fk_estado = estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarMedico` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarMedico`(in cedula_medicoP bigint)
begin select * from medico 
where cedula_medico=cedula_medicoP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarmedicoEstado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarmedicoEstado`(in nombre_estadoP varchar(50))
begin 
select
medico.`cedula_medico`AS CEDULA,
medico.`nombre_medico` AS NOMBRE,
medico.`apellido_medico` AS APELLIDO,
medico.`fecha_nacimiento` AS NACIMIENTO,
medico.`correo_medico`AS CORREO,
medico.`telefono_medico` AS TELEFONO,
medico.`direccion_medico` AS DIRECCION,
medico.`genero_medico` AS MEDICO,
estado.nombre_estado AS ESTADO
FROM	
medico,estado
WHERE
medico.fk_estado = estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarOperario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarOperario`(in cedula_operarioP bigint )
begin select * from operario
 where cedula_operario=cedula_operarioP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultaroperarioEstado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultaroperarioEstado`(in nombre_estadoP varchar(50))
begin 
select
operario.`cedula_operario`AS CEDULA,
operario.`nombre_operario` AS NOMBRE,
operario.`apellido_operario` AS APELLIDO,
operario.`fecha_nacimiento` AS NACIMIENTO,
operario.`correo_operario`AS CORREO,
operario.`telefono_operario` AS TELEFONO,
operario.`direccion_operario` AS DIRECCION,
operario.`genero_operario` AS OPERARIO,
estado.nombre_estado AS ESTADO
FROM	
operario,estado
WHERE
operario.fk_estado = estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarPaciente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarPaciente`(in cedula_pacienteP bigint)
begin select * from paciente
 where cedula_paciente= cedula_pacienteP ; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarpacienteEstado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarpacienteEstado`(in nombre_estadoP varchar(50))
begin 
select
paciente.`cedula_paciente`AS CEDULA,
paciente.`nombre_paciente` AS NOMBRE,
paciente.`apellido_paciente` AS APELLIDO,
paciente.`fecha_nacimiento` AS NACIMIENTO,
paciente.`correo_paciente`AS CORREO,
paciente.`telefono_paciente` AS TELEFONO,
paciente.`direccion_paciente` AS DIRECCION,
paciente.`genero_paciente` AS PACIENTE,
estado.nombre_estado AS ESTADO
FROM	
paciente,estado
WHERE
paciente.fk_estado = estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarProveedor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarProveedor`(in cedula_proveedorP bigint )
begin select * from proveedor
 where cedula_proveedor=cedula_proveedorP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Consultarproveedorcompras` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Consultarproveedorcompras`(in nombre_proveedorP varchar(50))
begin 
select
proveedor.`cedula_proveedor`AS CEDULA,
proveedor.`nombre_proveedor` AS NOMBRE,
proveedor.`apellido_proveedor` AS APELLIDO,
proveedor.`correo_proveedor`AS CORREO,
proveedor.`telefono_proveedor` AS TELEFONO,
 sum(detalle_factura_compra.cantidad_entrada) AS CANTIDAD_COMPRADA
FROM	
proveedor,estado,detalle_factura_compra
WHERE
proveedor.fk_estado = estado.codigo_estado AND
proveedor.nombre_proveedor = nombre_proveedorP ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConsultarproveedorEstado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarproveedorEstado`(in nombre_estadoP varchar(50))
begin 
select
proveedor.`cedula_proveedor`AS CEDULA,
proveedor.`nombre_proveedor` AS NOMBRE,
proveedor.`apellido_proveedor` AS APELLIDO,
proveedor.`correo_proveedor`AS CORREO,
proveedor.`telefono_proveedor` AS TELEFONO,
proveedor.`genero_proveedor` AS PROVEEDOR,
estado.nombre_estado AS ESTADO
FROM	
proveedor,estado
WHERE
proveedor.fk_estado = estado.codigo_estado AND
estado.nombre_estado = nombre_estadoP ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `EliminarAsignarInventario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarAsignarInventario`(in codigo_asignar_inventarioP bigint)
begin delete from asignar_inventario
where codigo_asignar_inventario = codigo_asignar_inventarioP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `EliminarCita` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarCita`(in codigo_citaP bigint)
begin delete from  cita
where codigo_cita=codigo_citaP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `EliminarDetalleFacturaCompra` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarDetalleFacturaCompra`(in codigo_detalle_factura_compraP bigint)
begin delete from  detalle_factura_compra
where  codigo_detalle_factura_compra = codigo_detalle_factura_compraP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `EliminarDetalleFacturaVenta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarDetalleFacturaVenta`(in codigo_detalle_factura_ventaP bigint)
begin delete from detalle_factura_venta
where codigo_detalle_factura_venta=codigo_detalle_factura_ventaP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `EliminarDiagnostico` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarDiagnostico`(in codigo_diagnosticoP bigint)
begin delete from  diagnostico
where codigo_diagnostico = codigo_diagnosticoP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `EliminarEstado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarEstado`(in codigo_estadooP bigint)
begin delete from estado
where codigo_estado=codigo_estadoP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `EliminarFacturaCompra` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarFacturaCompra`(in codigo_factura_compraP bigint)
begin delete from factura_compra
where codigo_factura_compra=codigo_factura_compraP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `EliminarFacturaVenta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarFacturaVenta`(in codigo_factura_ventaP bigint)
begin delete from factura_venta
where codigo_factura_venta= codigo_factura_ventaP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `EliminarFormaPago` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarFormaPago`(in codigo_forma_pagoP bigint)
begin delete from forma_pago
where codigo_forma_pago=codigo_forma_pagoP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `EliminarHistorial` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarHistorial`(in codigo_historialP bigint)
begin delete from historial
where codigo_historial=codigo_historialP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `EliminarInventarioMedicamento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarInventarioMedicamento`(in codigo_inventarioP bigint)
begin delete from inventario_medicamento
where codigo_inventario=codigo_inventarioP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `EliminarMedicamento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarMedicamento`(in codigo_medicamentoP bigint)
begin delete from medicamento
 where codigo_medicamento= codigo_medicamentoP ; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `EliminarMedico` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarMedico`(in cedula_medicoP bigint)
begin delete from medico 
where cedula_medico=cedula_medicoP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `EliminarOperario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarOperario`(in cedula_operarioP bigint )
begin delete from operario
 where cedula_operario=cedula_operarioP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `EliminarPaciente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarPaciente`(in cedula_pacienteP bigint)
begin delete from paciente
 where cedula_paciente= cedula_pacienteP ; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `EliminarProveedor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarProveedor`(in cedula_proveedorP bigint )
begin delete from proveedor
 where cedula_proveedor=cedula_proveedorP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ModificarAsignarInventario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ModificarAsignarInventario`(in codigo_asignar_inventarioP bigint, 
in cantidad_existenteP bigint, in stockP bigint, in fk_inventario_medicamentoP bigint,
in fk_medicamentoP bigint)
begin update asignar_inventario
set cantidad_existente=cantidad_existenteP, stock= stockP,  fk_inventario_medicamento=fk_inventario_medicamentoP,  
fk_medicamento= fk_medicamentoP where codigo_asignar_inventario = codigo_asignar_inventarioP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ModificarCita` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ModificarCita`(in codigo_citaP bigint, 
in fecha_citaP date, in hora_citaP time, in valor_citaP bigint,
in fk_operarioP bigint, in fk_pacienteP bigint, in fk_medicoP bigint,
in fk_estadoP bigint)
begin update cita
set fecha_cita= fecha_citaP, hora_cita = hora_citaP,  valor_cita = valor_citaP ,
fk_operario = fk_operarioP , fk_paciente = fk_pacienteP , fk_medico = fk_medicoP ,
fk_estado = fk_estadoP where codigo_cita=codigo_citaP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ModificarDetalleFacturaCompra` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ModificarDetalleFacturaCompra`(in codigo_detalle_factura_compraP bigint, 
in cantidad_entradaP bigint, in subtotalP bigint, in fk_factura_compraP bigint,
in fk_medicamentoP bigint)
begin update detalle_factura_compra
set
cantidad_entrada=cantidad_entradaP,  subtotal=subtotalP,  fk_factura_compra=fk_factura_compraP,
fk_medicamento= fk_medicamentoP where  codigo_detalle_factura_compra = codigo_detalle_factura_compraP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ModificarDetalleFacturaVenta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ModificarDetalleFacturaVenta`(in codigo_detalle_factura_ventaP bigint, 
in cantidad_salidaP bigint, in subtotalP bigint, in fk_factura_ventaP bigint,
in fk_medicamentoP bigint)
begin update detalle_factura_venta
set 
cantidad_salida = cantidad_salidaP,  subtotal = subtotalP, fk_factura_venta = fk_factura_ventaP,
fk_medicamento=fk_medicamentoP where codigo_detalle_factura_venta=codigo_detalle_factura_ventaP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ModificarDiagnostico` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ModificarDiagnostico`(in codigo_diagnosticoP bigint, 
in diagnostico_medicoP varchar(50), in diagnostico_enfermeriaP varchar(50), 
in fecha_diagnosticoP date, in fk_medicamentoP bigint,
in fk_medicoP bigint, in fk_estadoP bigint)
begin update diagnostico
set 
diagnostico_medico=diagnostico_medicoP,  diagnostico_enfermeria = diagnostico_enfermeriaP , 
fecha_diagnostico = fecha_diagnosticoP, fk_medicamento=fk_medicamentoP ,
fk_medico=fk_medicoP , fk_estado=fk_estadoP where codigo_diagnostico = codigo_diagnosticoP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ModificarEstado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ModificarEstado`(in codigo_estadoP bigint, 
in nombre_estadoP varchar(50))
begin update estado
set nombre_estado=nombre_estadoP 
where codigo_estado=codigo_estadoP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ModificarFacturaCompra` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ModificarFacturaCompra`(in codigo_factura_compraP bigint, 
in fecha_factura_compraP date, in total_compraP bigint, 
in fk_forma_pagoP bigint, in fk_operarioP bigint,
in fk_proveedorP bigint, in fk_estadoP bigint)
begin update factura_compra
set 
fecha_factura_compra = fecha_factura_compraP ,  total_compra = total_compraP,
fk_forma_pago= fk_forma_pagoP  , fk_operario=fk_operarioP  ,
fk_proveedor=fk_proveedorP  ,  fk_estado=fk_estadoP 
where codigo_factura_compra=codigo_factura_compraP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ModificarFacturaVenta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ModificarFacturaVenta`(in codigo_factura_ventaP bigint, 
in fecha_factura_ventaP date, in total_ventaP bigint, 
in fk_forma_pagoP bigint, in fk_citaP bigint, in fk_estadoP bigint)
begin update factura_venta
set 
fecha_factura_venta = fecha_factura_ventaP , total_venta= total_ventaP  ,
fk_forma_pago=fk_forma_pagoP  , fk_cita=fk_citaP,  fk_estado = fk_estadoP 
where codigo_factura_venta= codigo_factura_ventaP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ModificarFormaPago` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ModificarFormaPago`(in codigo_forma_pagoP bigint, 
in nombre_forma_pagoP varchar(50), in fk_estadoP bigint )
begin update forma_pago
set 
nombre_forma_pago = nombre_forma_pagoP ,  fk_estado = fk_estadoP 
where codigo_forma_pago=codigo_forma_pagoP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ModificarHistorial` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ModificarHistorial`(in codigo_historialP bigint, 
in antecedentesP varchar(50), in procedimientoActualP varchar(50),
in alergiasP varchar(50), in fk_medicoP bigint, 
in fk_pacienteP bigint, in fk_estadoP bigint )
begin update historial
set 
antecedentes = antecedentesP ,  procedimientoActual = procedimientoActualP ,
alergias = alergiasP , fk_medico = fk_medicoP , 
fk_paciente = fk_pacienteP , fk_estado = fk_estadoP 
where codigo_historial=codigo_historialP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ModificarInventarioMedicamento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ModificarInventarioMedicamento`(in codigo_inventarioP bigint, 
in nombre_inventarioP varchar(50), in fk_estadoP bigint )
begin update inventario_medicamento
set
nombre_inventario = nombre_inventarioP ,  fk_estado = fk_estadoP 
where codigo_inventario=codigo_inventarioP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ModificarMedicamento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ModificarMedicamento`(in codigo_medicamentoP bigint, 
in nombre_medicamentoP varchar(50), in descripcion_medicamentoP varchar(50),
in contraindicaciones_medicamentoP varchar(50), in sigla_medicamentoP varchar(50), 
in fecha_elaboracionP date, in fecha_vencimientoP date,
in valor_medicamentoP bigint, in fk_estadoP bigint )
begin update medicamento
set
 nombre_medicamento = nombre_medicamentoP ,  descripcion_medicamento = descripcion_medicamentoP ,
 contraindicaciones_medicamento = contraindicaciones_medicamentoP ,  sigla_medicamento = sigla_medicamentoP , 
 fecha_elaboracion = fecha_elaboracionP , fecha_vencimiento = fecha_vencimientoP ,
 valor_medicamento=valor_medicamentoP,    fk_estado = fk_estadoP
 where codigo_medicamento= codigo_medicamentoP ; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ModificarMedico` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ModificarMedico`(in cedula_medicoP bigint, 
in nombre_medicoP varchar(50), in apellido_medicoP varchar(50), 
in fecha_nacimientoP date, in correo_medicoP varchar(50), 
in telefono_medicoP bigint, in direccion_medicoP varchar(50),
in genero_medicoP varchar(50), in fk_estadoP bigint )
begin update medico
set
 nombre_medico = nombre_medicoP, apellido_medico = apellido_medicoP , 
 fecha_nacimiento = fecha_nacimientoP  ,  correo_medico = correo_medicoP , 
 telefono_medico = telefono_medicoP , direccion_medico = direccion_medicoP ,
 genero_medico = genero_medicoP ,  fk_estado = fk_estadoP  
where cedula_medico=cedula_medicoP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ModificarOperario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ModificarOperario`(in cedula_operarioP bigint, 
in nombre_operarioP varchar(50), in apellido_operarioP varchar(50), 
in fecha_nacimientoP date, in correo_operarioP varchar(50), 
in telefono_operarioP bigint, in direccion_operarioP varchar(50),
in genero_operarioP varchar(50), in fk_estadoP bigint )
begin update operario
set 
 nombre_operario = nombre_operarioP,    apellido_operario = apellido_operarioP , 
 fecha_nacimiento= fecha_nacimientoP  ,  correo_operario = correo_operarioP , 
 telefono_operario=telefono_operarioP  , direccion_operario = direccion_operarioP ,
 genero_operario = genero_operarioP ,  fk_estado = fk_estadoP  
 where cedula_operario=cedula_operarioP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ModificarPaciente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ModificarPaciente`(in cedula_pacienteP bigint, 
in nombre_pacienteP varchar(50), in apellido_pacienteP varchar(50), 
in fecha_nacimientoP date, in correo_pacienteP varchar(50), 
in telefono_pacienteP bigint, in direccion_pacienteP varchar(50),
in genero_pacienteP varchar(50), in fk_estadoP bigint )
begin update paciente
set
 nombre_paciente = nombre_pacienteP,   apellido_paciente=apellido_pacienteP  , 
 fecha_nacimiento=fecha_nacimientoP  ,  correo_paciente=correo_pacienteP   , 
 telefono_paciente=telefono_pacienteP  , direccion_paciente=direccion_pacienteP  ,
 genero_paciente=genero_pacienteP  ,  fk_estado=fk_estadoP  
 where cedula_paciente= cedula_pacienteP ; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ModificarProveedor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ModificarProveedor`(in cedula_proveedorP bigint, 
in nombre_proveedorP varchar(50), in apellido_proveedorP varchar(50), 
in correo_proveedorP varchar(50), 
in telefono_proveedorP bigint, 
in genero_proveedorP varchar(50), in fk_estadoP bigint )
begin update proveedor
set
 nombre_proveedor=nombre_proveedorP  ,  apellido_proveedor = apellido_proveedorP , 
 correo_proveedor = correo_proveedorP , 
 telefono_proveedor = telefono_proveedorP , 
 genero_proveedor = genero_proveedorP ,  fk_estado = fk_estadoP  
 where cedula_proveedor=cedula_proveedorP; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `vasignar_iventario`
--

/*!50001 DROP VIEW IF EXISTS `vasignar_iventario`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vasignar_iventario` AS select `asignar_inventario`.`codigo_asignar_inventario` AS `CODIGO`,`asignar_inventario`.`cantidad_existente` AS `CANTIDAD`,`asignar_inventario`.`stock` AS `STOCK`,`inventario_medicamento`.`nombre_inventario` AS `INVENTARIO`,`medicamento`.`nombre_medicamento` AS `MEDICAMENTO` from ((`asignar_inventario` join `inventario_medicamento`) join `medicamento`) where ((`asignar_inventario`.`fk_inventario_medicamento` = `inventario_medicamento`.`codigo_inventario`) and (`asignar_inventario`.`fk_medicamento` = `medicamento`.`codigo_medicamento`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vcita`
--

/*!50001 DROP VIEW IF EXISTS `vcita`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vcita` AS select `cita`.`codigo_cita` AS `CODIGO`,`cita`.`fecha_cita` AS `FECHA`,`cita`.`hora_cita` AS `HORA`,`cita`.`valor_cita` AS `VALOR`,`operario`.`nombre_operario` AS `OPERARIO`,`paciente`.`nombre_paciente` AS `PACIENTE`,`medico`.`nombre_medico` AS `MEDICO`,`estado`.`nombre_estado` AS `ESTADO` from ((((`cita` join `operario`) join `paciente`) join `medico`) join `estado`) where ((`cita`.`fk_operario` = `operario`.`cedula_operario`) and (`cita`.`fk_paciente` = `paciente`.`cedula_paciente`) and (`cita`.`fk_medico` = `medico`.`cedula_medico`) and (`cita`.`fk_estado` = `estado`.`codigo_estado`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vcitaactivo`
--

/*!50001 DROP VIEW IF EXISTS `vcitaactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vcitaactivo` AS select `cita`.`codigo_cita` AS `CODIGO`,`cita`.`fecha_cita` AS `FECHA`,`cita`.`hora_cita` AS `HORA`,`cita`.`valor_cita` AS `VALOR`,`operario`.`nombre_operario` AS `OPERARIO`,`paciente`.`nombre_paciente` AS `PACIENTE`,`medico`.`nombre_medico` AS `MEDICO`,`estado`.`nombre_estado` AS `ESTADO` from ((((`cita` join `operario`) join `paciente`) join `medico`) join `estado`) where ((`cita`.`fk_operario` = `operario`.`cedula_operario`) and (`cita`.`fk_paciente` = `paciente`.`cedula_paciente`) and (`cita`.`fk_medico` = `medico`.`cedula_medico`) and (`cita`.`fk_estado` = `estado`.`codigo_estado`) and (`cita`.`fk_estado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vcitainactivo`
--

/*!50001 DROP VIEW IF EXISTS `vcitainactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vcitainactivo` AS select `cita`.`codigo_cita` AS `CODIGO`,`cita`.`fecha_cita` AS `FECHA`,`cita`.`hora_cita` AS `HORA`,`cita`.`valor_cita` AS `VALOR`,`operario`.`nombre_operario` AS `OPERARIO`,`paciente`.`nombre_paciente` AS `PACIENTE`,`medico`.`nombre_medico` AS `MEDICO`,`estado`.`nombre_estado` AS `ESTADO` from ((((`cita` join `operario`) join `paciente`) join `medico`) join `estado`) where ((`cita`.`fk_operario` = `operario`.`cedula_operario`) and (`cita`.`fk_paciente` = `paciente`.`cedula_paciente`) and (`cita`.`fk_medico` = `medico`.`cedula_medico`) and (`cita`.`fk_estado` = `estado`.`codigo_estado`) and (`cita`.`fk_estado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vdetalle_factura_compra`
--

/*!50001 DROP VIEW IF EXISTS `vdetalle_factura_compra`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vdetalle_factura_compra` AS select `detalle_factura_compra`.`codigo_detalle_factura_compra` AS `CODIGO`,`detalle_factura_compra`.`cantidad_entrada` AS `CANTIDAD`,`detalle_factura_compra`.`subtotal` AS `SUBTOTAL`,`factura_compra`.`codigo_factura_compra` AS `NOFACTURA`,`medicamento`.`nombre_medicamento` AS `MEDICAMENTO` from ((`detalle_factura_compra` join `factura_compra`) join `medicamento`) where ((`detalle_factura_compra`.`fk_factura_compra` = `factura_compra`.`codigo_factura_compra`) and (`detalle_factura_compra`.`fk_medicamento` = `medicamento`.`codigo_medicamento`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vdetalle_factura_venta`
--

/*!50001 DROP VIEW IF EXISTS `vdetalle_factura_venta`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vdetalle_factura_venta` AS select `detalle_factura_venta`.`codigo_detalle_factura_venta` AS `CODIGO`,`detalle_factura_venta`.`cantidad_salida` AS `CANTIDAD`,`detalle_factura_venta`.`subtotal` AS `SUBTOTAL`,`factura_venta`.`codigo_factura_venta` AS `NOFACTURA`,`medicamento`.`nombre_medicamento` AS `MEDICAMENTO` from ((`detalle_factura_venta` join `factura_venta`) join `medicamento`) where ((`detalle_factura_venta`.`fk_factura_venta` = `factura_venta`.`codigo_factura_venta`) and (`detalle_factura_venta`.`fk_medicamento` = `medicamento`.`codigo_medicamento`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vdiagnostico`
--

/*!50001 DROP VIEW IF EXISTS `vdiagnostico`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vdiagnostico` AS select `diagnostico`.`codigo_diagnostico` AS `CODIGO`,`diagnostico`.`diagnostico_medico` AS `DIAGNOSTICO`,`diagnostico`.`diagnostico_enfermeria` AS `DESCRIPCION`,`diagnostico`.`fecha_diagnostico` AS `FECHA`,`medicamento`.`nombre_medicamento` AS `MEDICAMENTO`,`medico`.`nombre_medico` AS `MEDICO`,`estado`.`nombre_estado` AS `ESTADO` from (((`diagnostico` join `medicamento`) join `medico`) join `estado`) where ((`diagnostico`.`fk_medicamento` = `medicamento`.`codigo_medicamento`) and (`diagnostico`.`fk_medico` = `medico`.`cedula_medico`) and (`diagnostico`.`fk_estado` = `estado`.`codigo_estado`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vdiagnosticoactivo`
--

/*!50001 DROP VIEW IF EXISTS `vdiagnosticoactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vdiagnosticoactivo` AS select `diagnostico`.`codigo_diagnostico` AS `CODIGO`,`diagnostico`.`diagnostico_medico` AS `DIAGNOSTICO`,`diagnostico`.`diagnostico_enfermeria` AS `DESCRIPCION`,`diagnostico`.`fecha_diagnostico` AS `FECHA`,`medicamento`.`nombre_medicamento` AS `MEDICAMENTO`,`medico`.`nombre_medico` AS `MEDICO`,`estado`.`nombre_estado` AS `ESTADO` from (((`diagnostico` join `medicamento`) join `medico`) join `estado`) where ((`diagnostico`.`fk_medicamento` = `medicamento`.`codigo_medicamento`) and (`diagnostico`.`fk_medico` = `medico`.`cedula_medico`) and (`diagnostico`.`fk_estado` = `estado`.`codigo_estado`) and (`diagnostico`.`fk_estado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vdiagnosticoinactivo`
--

/*!50001 DROP VIEW IF EXISTS `vdiagnosticoinactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vdiagnosticoinactivo` AS select `diagnostico`.`codigo_diagnostico` AS `CODIGO`,`diagnostico`.`diagnostico_medico` AS `DIAGNOSTICO`,`diagnostico`.`diagnostico_enfermeria` AS `DESCRIPCION`,`diagnostico`.`fecha_diagnostico` AS `FECHA`,`medicamento`.`nombre_medicamento` AS `MEDICAMENTO`,`medico`.`nombre_medico` AS `MEDICO`,`estado`.`nombre_estado` AS `ESTADO` from (((`diagnostico` join `medicamento`) join `medico`) join `estado`) where ((`diagnostico`.`fk_medicamento` = `medicamento`.`codigo_medicamento`) and (`diagnostico`.`fk_medico` = `medico`.`cedula_medico`) and (`diagnostico`.`fk_estado` = `estado`.`codigo_estado`) and (`diagnostico`.`fk_estado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vestado`
--

/*!50001 DROP VIEW IF EXISTS `vestado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vestado` AS select `estado`.`codigo_estado` AS `CODIGO`,`estado`.`nombre_estado` AS `NOMBRE` from `estado` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vfactura_compra`
--

/*!50001 DROP VIEW IF EXISTS `vfactura_compra`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vfactura_compra` AS select `factura_compra`.`codigo_factura_compra` AS `CODIGO`,`factura_compra`.`fecha_factura_compra` AS `FECHACOMPRA`,`factura_compra`.`total_compra` AS `TOTAL`,`forma_pago`.`nombre_forma_pago` AS `FORMAPAGO`,`operario`.`nombre_operario` AS `OPERARIO`,`proveedor`.`nombre_proveedor` AS `PROVEEDOR`,`estado`.`nombre_estado` AS `ESTADO` from ((((`factura_compra` join `forma_pago`) join `operario`) join `proveedor`) join `estado`) where ((`factura_compra`.`fk_forma_pago` = `forma_pago`.`codigo_forma_pago`) and (`factura_compra`.`fk_operario` = `operario`.`cedula_operario`) and (`factura_compra`.`fk_proveedor` = `proveedor`.`cedula_proveedor`) and (`factura_compra`.`fk_estado` = `estado`.`codigo_estado`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vfactura_compraactivo`
--

/*!50001 DROP VIEW IF EXISTS `vfactura_compraactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vfactura_compraactivo` AS select `factura_compra`.`codigo_factura_compra` AS `CODIGO`,`factura_compra`.`fecha_factura_compra` AS `FECHACOMPRA`,`factura_compra`.`total_compra` AS `TOTAL`,`forma_pago`.`nombre_forma_pago` AS `FORMAPAGO`,`operario`.`nombre_operario` AS `OPERARIO`,`proveedor`.`nombre_proveedor` AS `PROVEEDOR`,`estado`.`nombre_estado` AS `ESTADO` from ((((`factura_compra` join `forma_pago`) join `operario`) join `proveedor`) join `estado`) where ((`factura_compra`.`fk_forma_pago` = `forma_pago`.`codigo_forma_pago`) and (`factura_compra`.`fk_operario` = `operario`.`cedula_operario`) and (`factura_compra`.`fk_proveedor` = `proveedor`.`cedula_proveedor`) and (`factura_compra`.`fk_estado` = `estado`.`codigo_estado`) and (`factura_compra`.`fk_estado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vfactura_comprainactivo`
--

/*!50001 DROP VIEW IF EXISTS `vfactura_comprainactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vfactura_comprainactivo` AS select `factura_compra`.`codigo_factura_compra` AS `CODIGO`,`factura_compra`.`fecha_factura_compra` AS `FECHACOMPRA`,`factura_compra`.`total_compra` AS `TOTAL`,`forma_pago`.`nombre_forma_pago` AS `FORMAPAGO`,`operario`.`nombre_operario` AS `OPERARIO`,`proveedor`.`nombre_proveedor` AS `PROVEEDOR`,`estado`.`nombre_estado` AS `ESTADO` from ((((`factura_compra` join `forma_pago`) join `operario`) join `proveedor`) join `estado`) where ((`factura_compra`.`fk_forma_pago` = `forma_pago`.`codigo_forma_pago`) and (`factura_compra`.`fk_operario` = `operario`.`cedula_operario`) and (`factura_compra`.`fk_proveedor` = `proveedor`.`cedula_proveedor`) and (`factura_compra`.`fk_estado` = `estado`.`codigo_estado`) and (`factura_compra`.`fk_estado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vfactura_venta`
--

/*!50001 DROP VIEW IF EXISTS `vfactura_venta`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vfactura_venta` AS select `factura_venta`.`codigo_factura_venta` AS `CODIGO`,`factura_venta`.`fecha_factura_venta` AS `FECHAVENTA`,`factura_venta`.`total_venta` AS `TOTAL`,`forma_pago`.`nombre_forma_pago` AS `FORMAPAGO`,`cita`.`codigo_cita` AS `NOCITA`,`estado`.`nombre_estado` AS `ESTADO` from (((`factura_venta` join `forma_pago`) join `cita`) join `estado`) where ((`factura_venta`.`fk_forma_pago` = `forma_pago`.`codigo_forma_pago`) and (`factura_venta`.`fk_cita` = `cita`.`codigo_cita`) and (`factura_venta`.`fk_estado` = `estado`.`codigo_estado`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vfactura_ventaactivo`
--

/*!50001 DROP VIEW IF EXISTS `vfactura_ventaactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vfactura_ventaactivo` AS select `factura_venta`.`codigo_factura_venta` AS `CODIGO`,`factura_venta`.`fecha_factura_venta` AS `FECHAVENTA`,`factura_venta`.`total_venta` AS `TOTAL`,`forma_pago`.`nombre_forma_pago` AS `FORMAPAGO`,`cita`.`codigo_cita` AS `NOCITA`,`estado`.`nombre_estado` AS `ESTADO` from (((`factura_venta` join `forma_pago`) join `cita`) join `estado`) where ((`factura_venta`.`fk_forma_pago` = `forma_pago`.`codigo_forma_pago`) and (`factura_venta`.`fk_cita` = `cita`.`codigo_cita`) and (`factura_venta`.`fk_estado` = `estado`.`codigo_estado`) and (`factura_venta`.`fk_estado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vfactura_ventainactivo`
--

/*!50001 DROP VIEW IF EXISTS `vfactura_ventainactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vfactura_ventainactivo` AS select `factura_venta`.`codigo_factura_venta` AS `CODIGO`,`factura_venta`.`fecha_factura_venta` AS `FECHAVENTA`,`factura_venta`.`total_venta` AS `TOTAL`,`forma_pago`.`nombre_forma_pago` AS `FORMAPAGO`,`cita`.`codigo_cita` AS `NOCITA`,`estado`.`nombre_estado` AS `ESTADO` from (((`factura_venta` join `forma_pago`) join `cita`) join `estado`) where ((`factura_venta`.`fk_forma_pago` = `forma_pago`.`codigo_forma_pago`) and (`factura_venta`.`fk_cita` = `cita`.`codigo_cita`) and (`factura_venta`.`fk_estado` = `estado`.`codigo_estado`) and (`factura_venta`.`fk_estado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vforma_pago`
--

/*!50001 DROP VIEW IF EXISTS `vforma_pago`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vforma_pago` AS select `forma_pago`.`codigo_forma_pago` AS `CODIGO`,`forma_pago`.`nombre_forma_pago` AS `NOMBRE`,`estado`.`nombre_estado` AS `ESTADO` from (`forma_pago` join `estado`) where (`forma_pago`.`fk_estado` = `estado`.`codigo_estado`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vforma_pagoactivo`
--

/*!50001 DROP VIEW IF EXISTS `vforma_pagoactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vforma_pagoactivo` AS select `forma_pago`.`codigo_forma_pago` AS `CODIGO`,`forma_pago`.`nombre_forma_pago` AS `NOMBRE`,`estado`.`nombre_estado` AS `ESTADO` from (`forma_pago` join `estado`) where ((`forma_pago`.`fk_estado` = `estado`.`codigo_estado`) and (`forma_pago`.`fk_estado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vforma_pagoinactivo`
--

/*!50001 DROP VIEW IF EXISTS `vforma_pagoinactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vforma_pagoinactivo` AS select `forma_pago`.`codigo_forma_pago` AS `CODIGO`,`forma_pago`.`nombre_forma_pago` AS `NOMBRE`,`estado`.`nombre_estado` AS `ESTADO` from (`forma_pago` join `estado`) where ((`forma_pago`.`fk_estado` = `estado`.`codigo_estado`) and (`forma_pago`.`fk_estado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vhistorial`
--

/*!50001 DROP VIEW IF EXISTS `vhistorial`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vhistorial` AS select `historial`.`codigo_historial` AS `CODIGO`,`historial`.`antecedentes` AS `ANTECEDENTES`,`historial`.`procedimientoActual` AS `PROCEDIMIENTO`,`historial`.`alergias` AS `ALERGIAS`,`medico`.`nombre_medico` AS `MEDICO`,`paciente`.`nombre_paciente` AS `PACIENTE`,`estado`.`nombre_estado` AS `ESTADO` from (((`historial` join `medico`) join `paciente`) join `estado`) where ((`historial`.`fk_medico` = `medico`.`cedula_medico`) and (`historial`.`fk_paciente` = `paciente`.`cedula_paciente`) and (`historial`.`fk_estado` = `estado`.`codigo_estado`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vhistorialactivo`
--

/*!50001 DROP VIEW IF EXISTS `vhistorialactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vhistorialactivo` AS select `historial`.`codigo_historial` AS `CODIGO`,`historial`.`antecedentes` AS `ANTECEDENTES`,`historial`.`procedimientoActual` AS `PROCEDIMIENTO`,`historial`.`alergias` AS `ALERGIAS`,`medico`.`nombre_medico` AS `MEDICO`,`paciente`.`nombre_paciente` AS `PACIENTE`,`estado`.`nombre_estado` AS `ESTADO` from (((`historial` join `medico`) join `paciente`) join `estado`) where ((`historial`.`fk_medico` = `medico`.`cedula_medico`) and (`historial`.`fk_paciente` = `paciente`.`cedula_paciente`) and (`historial`.`fk_estado` = `estado`.`codigo_estado`) and (`historial`.`fk_estado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vhistorialinactivo`
--

/*!50001 DROP VIEW IF EXISTS `vhistorialinactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vhistorialinactivo` AS select `historial`.`codigo_historial` AS `CODIGO`,`historial`.`antecedentes` AS `ANTECEDENTES`,`historial`.`procedimientoActual` AS `PROCEDIMIENTO`,`historial`.`alergias` AS `ALERGIAS`,`medico`.`nombre_medico` AS `MEDICO`,`paciente`.`nombre_paciente` AS `PACIENTE`,`estado`.`nombre_estado` AS `ESTADO` from (((`historial` join `medico`) join `paciente`) join `estado`) where ((`historial`.`fk_medico` = `medico`.`cedula_medico`) and (`historial`.`fk_paciente` = `paciente`.`cedula_paciente`) and (`historial`.`fk_estado` = `estado`.`codigo_estado`) and (`historial`.`fk_estado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vinventario_medicamento`
--

/*!50001 DROP VIEW IF EXISTS `vinventario_medicamento`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vinventario_medicamento` AS select `inventario_medicamento`.`codigo_inventario` AS `CODIGO`,`inventario_medicamento`.`nombre_inventario` AS `NOMBRE`,`estado`.`nombre_estado` AS `ESTADO` from (`inventario_medicamento` join `estado`) where (`inventario_medicamento`.`fk_estado` = `estado`.`codigo_estado`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vinventario_medicamentoactivo`
--

/*!50001 DROP VIEW IF EXISTS `vinventario_medicamentoactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vinventario_medicamentoactivo` AS select `inventario_medicamento`.`codigo_inventario` AS `CODIGO`,`inventario_medicamento`.`nombre_inventario` AS `NOMBRE`,`estado`.`nombre_estado` AS `ESTADO` from (`inventario_medicamento` join `estado`) where ((`inventario_medicamento`.`fk_estado` = `estado`.`codigo_estado`) and (`inventario_medicamento`.`fk_estado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vinventario_medicamentoinactivo`
--

/*!50001 DROP VIEW IF EXISTS `vinventario_medicamentoinactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vinventario_medicamentoinactivo` AS select `inventario_medicamento`.`codigo_inventario` AS `CODIGO`,`inventario_medicamento`.`nombre_inventario` AS `NOMBRE`,`estado`.`nombre_estado` AS `ESTADO` from (`inventario_medicamento` join `estado`) where ((`inventario_medicamento`.`fk_estado` = `estado`.`codigo_estado`) and (`inventario_medicamento`.`fk_estado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vmedicamento`
--

/*!50001 DROP VIEW IF EXISTS `vmedicamento`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vmedicamento` AS select `medicamento`.`codigo_medicamento` AS `CODIGO`,`medicamento`.`nombre_medicamento` AS `NOMBRE`,`medicamento`.`descripcion_medicamento` AS `DESCRIPCION`,`medicamento`.`contraindicaciones_medicamento` AS `CONTRAINDICACIONES`,`medicamento`.`sigla_medicamento` AS `SIGLA`,`medicamento`.`fecha_elaboracion` AS `ELABORADO`,`medicamento`.`fecha_vencimiento` AS `VENCE`,`medicamento`.`valor_medicamento` AS `VALOR`,`estado`.`nombre_estado` AS `ESTADO` from (`medicamento` join `estado`) where (`medicamento`.`fk_estado` = `estado`.`codigo_estado`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vmedicamentoactivo`
--

/*!50001 DROP VIEW IF EXISTS `vmedicamentoactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vmedicamentoactivo` AS select `medicamento`.`codigo_medicamento` AS `CODIGO`,`medicamento`.`nombre_medicamento` AS `NOMBRE`,`medicamento`.`descripcion_medicamento` AS `DESCRIPCION`,`medicamento`.`contraindicaciones_medicamento` AS `CONTRAINDICACIONES`,`medicamento`.`sigla_medicamento` AS `SIGLA`,`medicamento`.`fecha_elaboracion` AS `ELABORADO`,`medicamento`.`fecha_vencimiento` AS `VENCE`,`medicamento`.`valor_medicamento` AS `VALOR`,`estado`.`nombre_estado` AS `ESTADO` from (`medicamento` join `estado`) where ((`medicamento`.`fk_estado` = `estado`.`codigo_estado`) and (`medicamento`.`fk_estado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vmedicamentoinactivo`
--

/*!50001 DROP VIEW IF EXISTS `vmedicamentoinactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vmedicamentoinactivo` AS select `medicamento`.`codigo_medicamento` AS `CODIGO`,`medicamento`.`nombre_medicamento` AS `NOMBRE`,`medicamento`.`descripcion_medicamento` AS `DESCRIPCION`,`medicamento`.`contraindicaciones_medicamento` AS `CONTRAINDICACIONES`,`medicamento`.`sigla_medicamento` AS `SIGLA`,`medicamento`.`fecha_elaboracion` AS `ELABORADO`,`medicamento`.`fecha_vencimiento` AS `VENCE`,`medicamento`.`valor_medicamento` AS `VALOR`,`estado`.`nombre_estado` AS `ESTADO` from (`medicamento` join `estado`) where ((`medicamento`.`fk_estado` = `estado`.`codigo_estado`) and (`medicamento`.`fk_estado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vmedico`
--

/*!50001 DROP VIEW IF EXISTS `vmedico`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vmedico` AS select `medico`.`cedula_medico` AS `CEDULA`,`medico`.`nombre_medico` AS `NOMBRE`,`medico`.`apellido_medico` AS `APELLIDO`,`medico`.`fecha_nacimiento` AS `NACIMIENTO`,`medico`.`correo_medico` AS `CORREO`,`medico`.`telefono_medico` AS `TELEFONO`,`medico`.`direccion_medico` AS `DIRECCION`,`medico`.`genero_medico` AS `MEDICO`,`estado`.`nombre_estado` AS `ESTADO` from (`medico` join `estado`) where (`medico`.`fk_estado` = `estado`.`codigo_estado`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vmedicoactivo`
--

/*!50001 DROP VIEW IF EXISTS `vmedicoactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vmedicoactivo` AS select `medico`.`cedula_medico` AS `CEDULA`,`medico`.`nombre_medico` AS `NOMBRE`,`medico`.`apellido_medico` AS `APELLIDO`,`medico`.`fecha_nacimiento` AS `NACIMIENTO`,`medico`.`correo_medico` AS `CORREO`,`medico`.`telefono_medico` AS `TELEFONO`,`medico`.`direccion_medico` AS `DIRECCION`,`medico`.`genero_medico` AS `MEDICO`,`estado`.`nombre_estado` AS `ESTADO` from (`medico` join `estado`) where ((`medico`.`fk_estado` = `estado`.`codigo_estado`) and (`medico`.`fk_estado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vmedicoinactivo`
--

/*!50001 DROP VIEW IF EXISTS `vmedicoinactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vmedicoinactivo` AS select `medico`.`cedula_medico` AS `CEDULA`,`medico`.`nombre_medico` AS `NOMBRE`,`medico`.`apellido_medico` AS `APELLIDO`,`medico`.`fecha_nacimiento` AS `NACIMIENTO`,`medico`.`correo_medico` AS `CORREO`,`medico`.`telefono_medico` AS `TELEFONO`,`medico`.`direccion_medico` AS `DIRECCION`,`medico`.`genero_medico` AS `MEDICO`,`estado`.`nombre_estado` AS `ESTADO` from (`medico` join `estado`) where ((`medico`.`fk_estado` = `estado`.`codigo_estado`) and (`medico`.`fk_estado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `voperario`
--

/*!50001 DROP VIEW IF EXISTS `voperario`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `voperario` AS select `operario`.`cedula_operario` AS `CEDULA`,`operario`.`nombre_operario` AS `NOMBRE`,`operario`.`apellido_operario` AS `APELLIDO`,`operario`.`fecha_nacimiento` AS `NACIMIENTO`,`operario`.`correo_operario` AS `CORREO`,`operario`.`telefono_operario` AS `TELEFONO`,`operario`.`direccion_operario` AS `DIRECCION`,`operario`.`genero_operario` AS `OPERARIO`,`estado`.`nombre_estado` AS `ESTADO` from (`operario` join `estado`) where (`operario`.`fk_estado` = `estado`.`codigo_estado`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `voperarioactivo`
--

/*!50001 DROP VIEW IF EXISTS `voperarioactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `voperarioactivo` AS select `operario`.`cedula_operario` AS `CEDULA`,`operario`.`nombre_operario` AS `NOMBRE`,`operario`.`apellido_operario` AS `APELLIDO`,`operario`.`fecha_nacimiento` AS `NACIMIENTO`,`operario`.`correo_operario` AS `CORREO`,`operario`.`telefono_operario` AS `TELEFONO`,`operario`.`direccion_operario` AS `DIRECCION`,`operario`.`genero_operario` AS `OPERARIO`,`estado`.`nombre_estado` AS `ESTADO` from (`operario` join `estado`) where ((`operario`.`fk_estado` = `estado`.`codigo_estado`) and (`operario`.`fk_estado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `voperarioinactivo`
--

/*!50001 DROP VIEW IF EXISTS `voperarioinactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `voperarioinactivo` AS select `operario`.`cedula_operario` AS `CEDULA`,`operario`.`nombre_operario` AS `NOMBRE`,`operario`.`apellido_operario` AS `APELLIDO`,`operario`.`fecha_nacimiento` AS `NACIMIENTO`,`operario`.`correo_operario` AS `CORREO`,`operario`.`telefono_operario` AS `TELEFONO`,`operario`.`direccion_operario` AS `DIRECCION`,`operario`.`genero_operario` AS `OPERARIO`,`estado`.`nombre_estado` AS `ESTADO` from (`operario` join `estado`) where ((`operario`.`fk_estado` = `estado`.`codigo_estado`) and (`operario`.`fk_estado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vpaciente`
--

/*!50001 DROP VIEW IF EXISTS `vpaciente`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vpaciente` AS select `paciente`.`cedula_paciente` AS `CEDULA`,`paciente`.`nombre_paciente` AS `NOMBRE`,`paciente`.`apellido_paciente` AS `APELLIDO`,`paciente`.`fecha_nacimiento` AS `NACIMIENTO`,`paciente`.`correo_paciente` AS `CORREO`,`paciente`.`telefono_paciente` AS `TELEFONO`,`paciente`.`direccion_paciente` AS `DIRECCION`,`paciente`.`genero_paciente` AS `PACIENTE`,`estado`.`nombre_estado` AS `ESTADO` from (`paciente` join `estado`) where (`paciente`.`fk_estado` = `estado`.`codigo_estado`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vpacienteactivo`
--

/*!50001 DROP VIEW IF EXISTS `vpacienteactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vpacienteactivo` AS select `paciente`.`cedula_paciente` AS `CEDULA`,`paciente`.`nombre_paciente` AS `NOMBRE`,`paciente`.`apellido_paciente` AS `APELLIDO`,`paciente`.`fecha_nacimiento` AS `NACIMIENTO`,`paciente`.`correo_paciente` AS `CORREO`,`paciente`.`telefono_paciente` AS `TELEFONO`,`paciente`.`direccion_paciente` AS `DIRECCION`,`paciente`.`genero_paciente` AS `OPERARIO`,`estado`.`nombre_estado` AS `ESTADO` from (`paciente` join `estado`) where ((`paciente`.`fk_estado` = `estado`.`codigo_estado`) and (`paciente`.`fk_estado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vpacienteinactivo`
--

/*!50001 DROP VIEW IF EXISTS `vpacienteinactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vpacienteinactivo` AS select `paciente`.`cedula_paciente` AS `CEDULA`,`paciente`.`nombre_paciente` AS `NOMBRE`,`paciente`.`apellido_paciente` AS `APELLIDO`,`paciente`.`fecha_nacimiento` AS `NACIMIENTO`,`paciente`.`correo_paciente` AS `CORREO`,`paciente`.`telefono_paciente` AS `TELEFONO`,`paciente`.`direccion_paciente` AS `DIRECCION`,`paciente`.`genero_paciente` AS `OPERARIO`,`estado`.`nombre_estado` AS `ESTADO` from (`paciente` join `estado`) where ((`paciente`.`fk_estado` = `estado`.`codigo_estado`) and (`paciente`.`fk_estado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vproveedor`
--

/*!50001 DROP VIEW IF EXISTS `vproveedor`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vproveedor` AS select `proveedor`.`cedula_proveedor` AS `CEDULA`,`proveedor`.`nombre_proveedor` AS `NOMBRE`,`proveedor`.`apellido_proveedor` AS `APELLIDO`,`proveedor`.`correo_proveedor` AS `CORREO`,`proveedor`.`telefono_proveedor` AS `TELEFONO`,`proveedor`.`genero_proveedor` AS `PROVEEDOR`,`estado`.`nombre_estado` AS `ESTADO` from (`proveedor` join `estado`) where (`proveedor`.`fk_estado` = `estado`.`codigo_estado`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vproveedoractivo`
--

/*!50001 DROP VIEW IF EXISTS `vproveedoractivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vproveedoractivo` AS select `proveedor`.`cedula_proveedor` AS `CEDULA`,`proveedor`.`nombre_proveedor` AS `NOMBRE`,`proveedor`.`apellido_proveedor` AS `APELLIDO`,`proveedor`.`correo_proveedor` AS `CORREO`,`proveedor`.`telefono_proveedor` AS `TELEFONO`,`proveedor`.`genero_proveedor` AS `PROVEEDOR`,`estado`.`nombre_estado` AS `ESTADO` from (`proveedor` join `estado`) where ((`proveedor`.`fk_estado` = `estado`.`codigo_estado`) and (`proveedor`.`fk_estado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vproveedorinactivo`
--

/*!50001 DROP VIEW IF EXISTS `vproveedorinactivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vproveedorinactivo` AS select `proveedor`.`cedula_proveedor` AS `CEDULA`,`proveedor`.`nombre_proveedor` AS `NOMBRE`,`proveedor`.`apellido_proveedor` AS `APELLIDO`,`proveedor`.`correo_proveedor` AS `CORREO`,`proveedor`.`telefono_proveedor` AS `TELEFONO`,`proveedor`.`genero_proveedor` AS `PROVEEDOR`,`estado`.`nombre_estado` AS `ESTADO` from (`proveedor` join `estado`) where ((`proveedor`.`fk_estado` = `estado`.`codigo_estado`) and (`proveedor`.`fk_estado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-19 20:21:37
